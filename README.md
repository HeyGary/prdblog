系统全静态化。访问速度快。

可以自定义前台菜单。

系统底层框架是我自己编写的[https://git.oschina.net/zh/easyprd.git](https://git.oschina.net/zh/easyprd.git)

这套框架是很久以前练手写的。直接eclipse建项目。也没用maven什么的。

正准备把整个博客重构成rails版的。

线上访问地址：[小米粥的博客](http://www.prdblog.com/)


我的学习理念：重复造轮子。在造轮子中学到更多。

1、执行mysql.sql 初始化数据库。

2、登录系统
http://127.0.0.1:8080/prdblog/pages/login.jsp

用户名：admin 密码：admin

3、点击右上角《系统设置》修改相关配置。


添加文章时可自定义SEO相关关键字，标签等。

修改完成后请点击右上角重新生成页面。生成静态页面。
![前台主页](http://git.oschina.net/uploads/images/2015/0510/185544_d144893a_4313.png "前台博客主页")

![登录](http://git.oschina.net/uploads/images/2015/0510/185614_7bb3d5bd_4313.png "登录")

![后台主页](http://git.oschina.net/uploads/images/2015/0510/185709_e4a76c08_4313.png "后台主页")

![后台编辑页面](http://git.oschina.net/uploads/images/2015/0510/185741_da951526_4313.png "后台编辑页面")