-- MySQL dump 10.13  Distrib 5.6.17, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: prdblogdb
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `prd_accessory`
--

DROP TABLE IF EXISTS `prd_accessory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prd_accessory` (
  `id` varchar(100) NOT NULL,
  `aid` varchar(1000) DEFAULT NULL,
  `hash` varchar(1000) DEFAULT NULL,
  `address` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prd_accessory`
--

LOCK TABLES `prd_accessory` WRITE;
/*!40000 ALTER TABLE `prd_accessory` DISABLE KEYS */;
INSERT INTO `prd_accessory` VALUES ('67dc90a05bf54f3d91c668246500c9da','struts_1','prdblog/images/3888_35244_2.jpg?imageView/1/w/790/q/100|watermark/2/text/QHBvcnJpZGdl/font/Y2FsaWJyaQ==/fontsize/500/fill/IzAwMzVGRg==/dissolve/100/gravity/SouthEast/dx/10/dy/10','FnlCMDZCRZxwRaRLrdBlc-BVnFCT'),('a498e4c0527f4acf8c040512ffd3df08','c1fd559d28884e3daf0e4c0c52846301','prdblog/images/11742_25185_QQ截图20140513225332.jpg?imageView/1/w/790/q/100|watermark/2/text/QHBvcnJpZGdl/font/Y2FsaWJyaQ==/fontsize/500/fill/IzAwMzVGRg==/dissolve/100/gravity/SouthEast/dx/10/dy/10','FsVo8XBtuj3-QhDRuvNDLI_iD4IQ'),('bfcdf2e511cb406992a24584146b374b','struts_1','prdblog/images/8893_22348_3.jpg?imageView/1/w/790/q/100|watermark/2/text/QHBvcnJpZGdl/font/Y2FsaWJyaQ==/fontsize/500/fill/IzAwMzVGRg==/dissolve/100/gravity/SouthEast/dx/10/dy/10','FkjO_6hXqmsqUkerSgqj8kfq2l7D'),('f28fe6a156034bdea4de6686be665dd9','struts_1','prdblog/images/11712_20852_1.jpg?imageView/1/w/790/q/100|watermark/2/text/QHBvcnJpZGdl/font/Y2FsaWJyaQ==/fontsize/500/fill/IzAwMzVGRg==/dissolve/100/gravity/SouthEast/dx/10/dy/10','Fjpqd_jh_v_bIPSt8THXbrIpQLwI');
/*!40000 ALTER TABLE `prd_accessory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prd_article`
--

DROP TABLE IF EXISTS `prd_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prd_article` (
  `id` varchar(100) NOT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `atr_content` text,
  `cls` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `seo` varchar(1000) DEFAULT NULL,
  `sendtime` varchar(100) DEFAULT NULL,
  `accid` varchar(3000) DEFAULT NULL,
  `lablename` varchar(3000) DEFAULT NULL,
  `labletype` varchar(1000) DEFAULT NULL,
  `adduserid` varchar(1000) DEFAULT NULL,
  `addusername` varchar(1000) DEFAULT NULL,
  `breviary` text,
  `catalog` text,
  `top` varchar(100) DEFAULT NULL,
  `tid` varchar(1000) DEFAULT NULL,
  `keywords` varchar(1000) DEFAULT NULL,
  `discuss` varchar(2000) DEFAULT NULL,
  `arttext` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prd_article`
--

LOCK TABLES `prd_article` WRITE;
/*!40000 ALTER TABLE `prd_article` DISABLE KEYS */;
INSERT INTO `prd_article` VALUES ('1971435b99a1407f967cd4a131a6b41b','prdblog博客正式建立啦','<p>个人网站正式启用。主要记录技术方面文章。希望能与大家一同成长。</p><p><br/></p><p>意见、建议请直接Q我。或者邮箱我。<img src=\"http://porridge.qiniudn.com/prdblog/images/11742_25185_QQ截图20140513225332.jpg\" width=\"178\" height=\"32\" border=\"0\" hspace=\"0\" vspace=\"0\" title=\"\" style=\"width: 178px; height: 32px;\"/></p>','00030003','0','prdblog博客,prdblog,porridge','2014-05-13 22:07:33','','说说而已,','1','001','admin','<p>个人网站正式启用。主要记录技术方面文章。希望能与大家一同成长。</p><p><br/></p><p>意见、建议请直接Q我。或者邮箱我。<img src=\"http://porridge.qiniudn.com/prdblog/images/11742_25185_QQ截图20140513225332.jpg\" width=\"178\" height=\"32\" border=\"0\" hspace=\"0\" vspace=\"0\" title=\"\" style=\"width: 178px; height: 32px;\"/></p>...',NULL,'','','prdblog博客正式建立啦','true',NULL),('67dad3860b744006b0866ac0f2d925e5','我的实验室','<p>&nbsp;</p><p>&nbsp;</p><p>实验室主要放我平时做一些测试的DEMO自己参考一下。也供大家参考参考。</p><p>&nbsp;</p><p>实验室第一个实验对象</p><h1>SVNKIT</h1><p><a href=\"http://www.prdblog.com/html/index/svnkit.html\" _src=\"http://www.prdblog.com/html/index/svnkit.html\">http://www.prdblog.com/html/index/svnkit.html</a></p><p><br/></p><h1>EasyPrd</h1><p><a href=\"http://www.prdblog.com/html/index/easyprd.html\" _src=\"http://www.prdblog.com/html/index/easyprd.html\">http://www.prdblog.com/html/index/easyprd.html</a> </p><p><br/></p>','0004','0','svn打包,svn打包项目,java读取svn','2014-04-30 21:54:10','','','0','001','admin','<p>&nbsp;</p><p>&nbsp;</p><p>实验室主要放我平时做一些测试的DEMO自己参考一下。也供大家参考参考。</p><p>&nbsp;</p><p>实验室第一个实验对象</p><h1>SVNKIT</h1><p><a href=\"http://www.prdblog.com/html/index/svnkit.html\" _src=\"http://www.prdblog.com/html/index/svnkit.html\">http://www.prdblog.com/html/index/svnkit.html</a></p>...','<dt eq=\"0\"><span>1</span>SVNKIT</dt><dt eq=\"1\"><span>2</span>EasyPrd</dt>',NULL,'','svn打包,svn打包项目,java如何读取svn','true',''),('c1fd559d28884e3daf0e4c0c52846301','关于我','<h1>关于我</h1><p><span style=\"font-size: 20px;\"><br/></span></p><p><strong>小米粥/porridge</strong></p><p><br/></p><p>男</p><p><br/></p><p>爱生活,爱代码.</p><p><br/></p><p>喜欢自己捣鼓一些东西.有不错的自学能力.</p><p><br/></p><p>感兴趣?请联系.</p><p><img src=\"http://porridge.qiniudn.com/prdblog/images/11742_25185_QQ%E6%88%AA%E5%9B%BE20140513225332.jpg\" title=\"\" width=\"178\" height=\"32\" border=\"0\" hspace=\"0\" vspace=\"0\" style=\"width: 178px; height: 32px;\"/></p><p><br/></p><p><br/></p><h1>关于网站</h1><p>网站使用技术：后台，自写了一套转发以及DAO框架。前台，PJAX，手工写了一些HTML+CSS</p><p>网站正式启用于2014-06-09日。备案了一个月时间正式启用。<br/></p><p>没有华丽的美工，只有简单的一些文章，<br/></p><p>记录自己走过的点点滴滴。</p><p>技术，心情。<br/></p><p style=\"text-align: right;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 书于:2014-06-12</p><p><br/></p><p><br/></p>','0006','0','小米粥自我介绍  this is porridge','2014-04-26 17:01:30','prdblog/images/11742_25185_QQ截图20140513225332.jpg?imageView/1/w/790/q/100|watermark/2/text/QHBvcnJpZGdl/font/Y2FsaWJyaQ==/fontsize/500/fill/IzAwMzVGRg==/dissolve/100/gravity/SouthEast/dx/10/dy/10####FsVo8XBtuj3-QhDRuvNDLI_iD4IQ,','','0','001','admin','<h1>关于我</h1><p><span style=\"font-size: 20px;\"><br/></span></p><p><strong>小米粥/porridge</strong></p><p><br/></p><p>男</p><p><br/></p><p>爱生活,爱代码.</p><p><br/></p><p>喜欢自己捣鼓一些东西.有不错的自学能力.</p><p><br/></p><p>感兴趣?请联系.</p>...','<dt eq=\"0\"><span>1</span>关于我</dt><dt eq=\"1\"><span>2</span>关于网站</dt>',NULL,'','小米粥的自我介绍','false',''),('cf9e816a2aac41aa9809e45cd9bdba42','小米粥','<p>小米粥的个人简历：</p><p><br/></p><p>周会 | 男 | 22岁 | 二年工作经验</p><p><br/></p><p>我的标准版简历下载：</p><p><br/></p><p>doc版：<a href=\"http://porridge.qiniudn.com/downloadzhouhui.doc\" _src=\"http://porridge.qiniudn.com/downloadzhouhui.doc\">http://porridge.qiniudn.com/downloadzhouhui.doc</a> </p><p><br/></p><p>PDF版：<a href=\"http://porridge.qiniudn.com/downloadzhouhui.pdf\" _src=\"http://porridge.qiniudn.com/downloadzhouhui.pdf\">http://porridge.qiniudn.com/downloadzhouhui.pdf</a> </p><p><br/></p><p>图片版：<a href=\"http://porridge.qiniudn.com/downloadzhouhui.png\" _src=\"http://porridge.qiniudn.com/downloadzhouhui.png\">http://porridge.qiniudn.com/downloadzhouhui.png</a> </p><p><br/></p><p>如果合适？欢迎预约面试。</p>','0007','0','小米粥','2014-09-21 10:01:36','','','0','001','admin','<p>小米粥的个人简历：</p><p><br/></p><p>周会 | 男 | 22岁 | 二年工作经验</p><p><br/></p><p>我的标准版简历下载：</p><p><br/></p><p>doc版：<a href=\"http://porridge.qiniudn.com/downloadzhouhui.doc\" _src=\"http://porridge.qiniudn.com/downloadzhouhui.doc\">http://porridge.qiniudn.com/downloadzhouhui.doc</a> </p>...',NULL,'','','小米粥的个人简历','true',''),('jqgrid','jqgrid简单封装','<p>序:找了许多插件发现jqgrid插件蛮不错的.但是比较烦写js觉得麻烦.所以简单的封装一下让他们自动生成.</p>','00030002','0','jqgrid一次封装,html标记式表格','2014-06-10 22:11:44','','jqgrid封装,','1','001','admin','<p>序:找了许多插件发现jqgrid插件蛮不错的.但是比较烦写js觉得麻烦.所以简单的封装一下让他们自动生成.</p>',NULL,'','jqgrid','jqgrid简单封装一下以HTML方式调用.','true',''),('struts_1','struts2源代码解读','<p>hello.</p>','00030001','0','struts2源代码解读','2014-06-09 21:36:08','prdblog/images/11712_20852_1.jpg?imageView/1/w/790/q/100|watermark/2/text/QHBvcnJpZGdl/font/Y2FsaWJyaQ==/fontsize/500/fill/IzAwMzVGRg==/dissolve/100/gravity/SouthEast/dx/10/dy/10####Fjpqd_jh_v_bIPSt8THXbrIpQLwI,prdblog/images/3888_35244_2.jpg?imageView/1/w/790/q/100|watermark/2/text/QHBvcnJpZGdl/font/Y2FsaWJyaQ==/fontsize/500/fill/IzAwMzVGRg==/dissolve/100/gravity/SouthEast/dx/10/dy/10####FnlCMDZCRZxwRaRLrdBlc-BVnFCT,prdblog/images/8893_22348_3.jpg?imageView/1/w/790/q/100|watermark/2/text/QHBvcnJpZGdl/font/Y2FsaWJyaQ==/fontsize/500/fill/IzAwMzVGRg==/dissolve/100/gravity/SouthEast/dx/10/dy/10####FkjO_6hXqmsqUkerSgqj8kfq2l7D,','struts源代码解读,','1','001','admin','<p>hello.</p>','<dt eq=\"0\"><span>1</span>一、准备必要的环境</dt><dt eq=\"1\"><span>2</span>二、搭建struts2运行环镜</dt><dd eq=\"0\"><span>2.1</span>&nbsp; &nbsp; 新建struts2的项目</dd><dd eq=\"1\"><span>2.2</span>&nbsp; web.xml添加拦截器</dd><dd eq=\"2\"><span>2.3</span>创建测试action</dd><dd eq=\"3\"><span>2.4</span>添加struts2.xml</dd><dd eq=\"4\"><span>2.5</span>启动测试项目.</dd><dt eq=\"2\"><span>3</span>三、搭建调试struts2源代码环境</dt>','','struts_1',',eclipse搭建struts2源代码调试环境研究struts2源代码.','true','');
/*!40000 ALTER TABLE `prd_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prd_label`
--

DROP TABLE IF EXISTS `prd_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prd_label` (
  `id` varchar(100) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `addtime` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prd_label`
--

LOCK TABLES `prd_label` WRITE;
/*!40000 ALTER TABLE `prd_label` DISABLE KEYS */;
INSERT INTO `prd_label` VALUES ('2553f7861a2a41ef93ecfd0009dc7414','java插件','2014-08-25'),('263e3706feb94bac9121839e996f1a7f','java基础','2014-08-13'),('28ccd15cec25449786e1184c4ad836a8','java多线程','2014-09-30'),('385928056f9947188825dbdef0f6b8a9','database','2015-01-09'),('5c8eda0955154825b0bcd13315686b03','java源代码','2014-08-13'),('67b5b7d0a4c54fc2be23dab538239cff','WebSocket','2014-07-09'),('693467ba4bca498fa183aaeeda0c3e21','说说而已','2014-05-13'),('80f7ed09b68e4caeb18f3cda669d5389','struts源代码解读','2014-06-09'),('8108951669524371970230512aebd662','pjax','2014-06-12'),('b6c6eff405034ec2a6dd5c5431f021d6','EasyPrd','2014-08-27'),('fd55ca97b24f4500ac67cfdaf0fa7f3d','jqgrid封装','2014-06-10');
/*!40000 ALTER TABLE `prd_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prd_labelart`
--

DROP TABLE IF EXISTS `prd_labelart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prd_labelart` (
  `id` varchar(100) NOT NULL,
  `aid` varchar(1000) DEFAULT NULL,
  `lid` varchar(1000) DEFAULT NULL,
  `addtime` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prd_labelart`
--

LOCK TABLES `prd_labelart` WRITE;
/*!40000 ALTER TABLE `prd_labelart` DISABLE KEYS */;
INSERT INTO `prd_labelart` VALUES ('09f1930f2ec24a3aaff45ebb945ace74','jqgrid','fd55ca97b24f4500ac67cfdaf0fa7f3d','2015-05-10'),('40cfa0035a0148fb9a7711ec8f8a197e','1971435b99a1407f967cd4a131a6b41b','693467ba4bca498fa183aaeeda0c3e21','2014-06-12'),('5ee3bf829ea44af681288cdd8eccbca3','struts_1','80f7ed09b68e4caeb18f3cda669d5389','2015-05-10');
/*!40000 ALTER TABLE `prd_labelart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prd_link`
--

DROP TABLE IF EXISTS `prd_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prd_link` (
  `id` varchar(200) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `path` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prd_link`
--

LOCK TABLES `prd_link` WRITE;
/*!40000 ALTER TABLE `prd_link` DISABLE KEYS */;
INSERT INTO `prd_link` VALUES ('901588b756394ec98032056638d29023','交换友连','http://www.prdblog.com/html/list/0006.html');
/*!40000 ALTER TABLE `prd_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_fun`
--

DROP TABLE IF EXISTS `sys_fun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_fun` (
  `id` varchar(200) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `parentid` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `noteid` varchar(1000) DEFAULT NULL,
  `cls` varchar(1000) DEFAULT NULL,
  `link` varchar(1000) DEFAULT NULL,
  `funcode` varbinary(2000) NOT NULL,
  `ftltype` varchar(2000) DEFAULT NULL,
  `ftlpath` varchar(2000) DEFAULT NULL,
  `sort` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_fun`
--

LOCK TABLES `sys_fun` WRITE;
/*!40000 ALTER TABLE `sys_fun` DISABLE KEYS */;
INSERT INTO `sys_fun` VALUES ('02643c35ef4e46ddbf492bd55d1c9fd5','修改','540d9fe3bbae4b3f8e39be41640233e2','2','','btn','label.loadlabel()','000100040002',NULL,NULL,'0003'),('10c20b49a0504dd4bb37582836f9beea','排序','62d7620804404d05b8f15c6c6a0b4713','2','','btn','fun.sort()','000100020004',NULL,NULL,'0005'),('18539f5de503441e83cb2ce521471f92','添加','b4aa2c98368f4441919c424df3f682ae','2','','btn','list.add()','000300020001',NULL,NULL,'0002'),('1a1295f5a7974702bfb88bd96ad3342e','修改','70a2ae2365bb498496f74e09a8e23a51','2','','btn','list.update()','000300030002',NULL,NULL,'000300030002'),('257590d5601447238314e1326a0d8bf1','删除','7d7e63b7c66b409ea4e9c1c89e1a1e07','2','','btn','list.del()','000300010003',NULL,'','0004'),('39d304e02e6248a4abae1c59ddd73cc4','查看','7d7e63b7c66b409ea4e9c1c89e1a1e07','2','','btn','list.find()','000300010004',NULL,NULL,'0005'),('49c34684a0814995a3ba9d2a50b25ecd','删除','8e0cfe2d618d4d6d959d16a2d93e82fe','2','','btn','user.deluser()','000100010003',NULL,NULL,'0004'),('51df87628110437c8d058e1640f7e1f7','关于','','3','','icon-user','/pages/background/data/add.jsp','0006','1','alone.ftl','0006'),('540d9fe3bbae4b3f8e39be41640233e2','标签管理','db3e11e722b14170acf27c3f28694eed','1','','','/pages/background/data/label.jsp','00010004',NULL,NULL,'0002'),('56a953606e734a37bae18fa89e1ec6a4','删除','f3c2241191184c56a3cd94eb3ce3f271','2','','btn','link.dellink()','000100050003',NULL,NULL,'000100050003'),('5f1b20376ce0435996e6291605800864','修改','b4aa2c98368f4441919c424df3f682ae','2','','btn','list.update()','000300020002',NULL,NULL,'0003'),('5f1fa0add6f144ff9be503bf3440591c','删除','b4aa2c98368f4441919c424df3f682ae','2','','btn','list.del()','000300020003',NULL,NULL,'0004'),('608275bee3db469aab02e153b971e75f','删除','540d9fe3bbae4b3f8e39be41640233e2','2','','btn','label.dellabel()','000100040003',NULL,NULL,'0004'),('62d7620804404d05b8f15c6c6a0b4713','功能管理','db3e11e722b14170acf27c3f28694eed','1','','','/pages/background/system/fun.jsp','00010002',NULL,NULL,'0003'),('70a2ae2365bb498496f74e09a8e23a51','杂谈','af67d8e180bf450a9baaa6a08062e286','3','','','/pages/background/data/list.jsp','00030003','2','index.ftl','00030003'),('72016e81c4e7448ba704bebb2546ece9','添加','540d9fe3bbae4b3f8e39be41640233e2','2','','btn','label.toggle(1)','000100040001',NULL,NULL,'0002'),('7d7e63b7c66b409ea4e9c1c89e1a1e07','后台笔记','af67d8e180bf450a9baaa6a08062e286','3','','','/pages/background/data/list.jsp','00030001','2','index.ftl',NULL),('84644aa8ef8d46e89f1278ad4f0d4920','修改','62d7620804404d05b8f15c6c6a0b4713','2','','btn','fun.loadfun()','000100020002',NULL,NULL,'0003'),('8c0378ec969f4290827c22e2c8abde7e','修改','8e0cfe2d618d4d6d959d16a2d93e82fe','2','','btn','user.loaduser()','000100010002',NULL,NULL,'0003'),('8e0cfe2d618d4d6d959d16a2d93e82fe','用户管理','db3e11e722b14170acf27c3f28694eed','1','','','/pages/background/system/user.jsp','00010001',NULL,NULL,'0004'),('970e6f68d4494a319e0ccee2a79490fc','删除','62d7620804404d05b8f15c6c6a0b4713','2','','btn','fun.delfun()','000100020003',NULL,NULL,'0004'),('ab1aaba660be4340972a2dd76ef6e78c','添加','7d7e63b7c66b409ea4e9c1c89e1a1e07','2','','btn','list.add()','000300010001',NULL,'','0002'),('af67d8e180bf450a9baaa6a08062e286','笔记','','3','','icon-tags','','0003','0','','0003'),('b4aa2c98368f4441919c424df3f682ae','前端笔记','af67d8e180bf450a9baaa6a08062e286','3','','','/pages/background/data/list.jsp','00030002','2','index.ftl',NULL),('b730a8163b204f94b7fb3d2fac37a4f1','添加','70a2ae2365bb498496f74e09a8e23a51','2','','btn','list.add()','000300030001',NULL,NULL,'000300030001'),('bc560671001f4098b855b33f0e86d969','实验室','','3','','icon-briefcase','/pages/background/data/add.jsp','0004','1','alone.ftl','0004'),('bdac74b416c14b6d87a85251e2eeb618','查看','b4aa2c98368f4441919c424df3f682ae','2','','btn','list.find()','000300020004',NULL,NULL,'0005'),('c7f3148d6bba4185af0506493c902bfe','添加','f3c2241191184c56a3cd94eb3ce3f271','2','','btn','link.toggle(1)','000100050001',NULL,NULL,'000100050001'),('c8e617b0a28e433bac7c08d40e574110','删除','70a2ae2365bb498496f74e09a8e23a51','2','','btn','list.del()','000300030003',NULL,NULL,'000300030003'),('cf504b3434a54d97baa79a60d1e003e3','修改','7d7e63b7c66b409ea4e9c1c89e1a1e07','2','','btn','list.update()','000300010002',NULL,'','0003'),('d0aa5c1987b748049acada4957f4c146','查看','70a2ae2365bb498496f74e09a8e23a51','2','','btn','list.find()','000300030004',NULL,NULL,'000300030004'),('db3e11e722b14170acf27c3f28694eed','系统设置','','0','','icon-legal','','0001',NULL,NULL,'0007'),('e14b079a5f9246799d7140d0953b41f0','添加','8e0cfe2d618d4d6d959d16a2d93e82fe','2','','btn','user.toggle(1)','000100010001',NULL,NULL,'0002'),('e56eacb51c26481f81c4e0220ef13347','添加','62d7620804404d05b8f15c6c6a0b4713','2','','btn','fun.toggle(1)','000100020001',NULL,NULL,'0002'),('f39be818a8cc46b496b41b616bcd4c5d','修改','f3c2241191184c56a3cd94eb3ce3f271','2','','btn','link.linkload()','000100050002',NULL,NULL,'000100050002'),('f3c2241191184c56a3cd94eb3ce3f271','友情连接','db3e11e722b14170acf27c3f28694eed','1','','','/pages/background/data/link.jsp','00010005',NULL,NULL,'0005'),('index','主页','','3','','icon-home','','0002','3','index.ftl','0002');
/*!40000 ALTER TABLE `sys_fun` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `id` varchar(200) NOT NULL,
  `username` varchar(2000) DEFAULT NULL,
  `realname` varchar(2000) DEFAULT NULL,
  `pasword` varchar(2000) DEFAULT NULL,
  `unitid` varchar(2000) DEFAULT NULL,
  `address` varchar(2000) DEFAULT NULL,
  `phone` varchar(2000) DEFAULT NULL,
  `mail` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES ('001','admin','admin','21232F297A57A5A743894A0E4A801FC3','001','Chain','13726288267','mfkwfc@qq.com');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_label`
--

DROP TABLE IF EXISTS `v_label`;
/*!50001 DROP VIEW IF EXISTS `v_label`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_label` (
  `id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `addtime` tinyint NOT NULL,
  `number` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_label`
--

/*!50001 DROP TABLE IF EXISTS `v_label`*/;
/*!50001 DROP VIEW IF EXISTS `v_label`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_label` AS select `l`.`id` AS `id`,`l`.`name` AS `name`,`l`.`addtime` AS `addtime`,(select count(1) from `prd_labelart` where (`prd_labelart`.`lid` = `l`.`id`)) AS `number` from `prd_label` `l` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-10 18:03:25
