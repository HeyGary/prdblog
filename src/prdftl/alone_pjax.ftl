<title>${art.title}</title>
<div id="article" class="alone">${art.atr_content}
<#if art.arttext??>${art.arttext}</#if>
<#if art.catalog??>
<dl id="catalog">${art.catalog}</dl>
</#if>
<#if art.discuss?? && art.discuss=='true'>
<div class="duoshuo_hr"></div>
<div class="ds-thread" data-thread-key="${art.id}" data-title="${art.title}" data-url="http://www.prdblog.com/${writeurl}/${art.id}.html"></div>
</#if>
</div>