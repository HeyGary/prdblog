<meta name="author" content="prridge" />
<meta name="copyright" content="© http://www.prdblog.com" />
<link rel="shortcut icon" href="${projname}/html/resource/images/porridge.ico" type="image/vnd.microsoft.icon" />
<link rel="stylesheet" type="text/css" href="${projname}/html/resource/style.css">
<link type="text/css" rel="stylesheet" href="${projname}/resource/js/syntax-highlighter/styles/shCore.css"/>
<link type="text/css" rel="stylesheet" href="${projname}/resource/js/syntax-highlighter/styles/shThemeDefault.css"/>
<!--[if lt IE 8]>
<div style="width:1080px;margin:0 auto;">
<span style="color:red;font-size:14px;">您所使用的浏览器版本过低,为了您更舒适的浏览网页!建议升级成高端大气上档气的浏览器</span>
<br/>  chorme(谷歌浏览器):<a target="_blank" href="http://www.google.cn/intl/zh-CN/chrome/browser/">新窗口打开下载</a><br/>firefox(火狐浏览器):<a target="_blank" href="http://www.firefox.com.cn/">新窗口打开下载</a>
</div>
<![endif]-->