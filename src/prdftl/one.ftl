<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${sysuser} ${art.title}</title>
<#if art.seo??><meta name="description" content="${sysuser},${art.seo}"/></#if>
<#if art.keywords??><meta name="keywords" content="${art.keywords}"/></#if>
<#include '/loadbase/base.ftl'>
</head>
<body>
<#include '/loadbase/navbar.ftl'>
<!-- 内容 -->
<div class="main clearfix">
<div id="content" class="content">
<#include '/base/article.ftl'>
</div>
<#include '/loadbase/right.ftl'>
</div>
<#include '/loadbase/floor.ftl'>
</body>
</html>