<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${sysuser} ${topname}</title>
<meta name="description" content="${sysseo}"/>
<meta name="keywords" content="${sysuser},${sysseo}"/>
<#include '/loadbase/base.ftl'>
</head>
<body>
<#include '/loadbase/navbar.ftl'>
<div class="main clearfix">
<div id="content" class="content">
<h2 class="hint">${topname}</h2>
<#include '/base/list.ftl'>
</div>
<#include '/loadbase/right.ftl'>
</div>
<#include '/loadbase/floor.ftl'>
</body>
</html>