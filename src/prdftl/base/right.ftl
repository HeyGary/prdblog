<div class="right_menu">
	<div class="category">
	<div class="title">标签分类</div>
	<div class="label">
	<#list fts as f>
		<a title="${sysuser}的${f.name}分类内共有${f.number}篇文章记录" href="${projname}${f.link}">${f.name}</a>
	</#list>
	</div>
	</div>
	<div class="category">
	<div class="title">日志归档</div>
	<div id="mod_calendar" datajson='${ftstimejson}' class="mod_calendar">
		<div class="calendar_year">
		<a class="calendar_prev"></a>
		<strong class="year">${firstdate}年</strong>
		<a class="calendar_next"></a>
		</div>
		<ul class="clearfix calendar_month">
			<li title="${sysuser}的一月份文章统计汇总记录">1月</li>
			<li title="${sysuser}的二月份文章统计汇总记录">2月</li>
			<li title="${sysuser}的三月份文章统计汇总记录">3月</li>
			<li title="${sysuser}的四月份文章统计汇总记录">4月</li>
			<li title="${sysuser}的五月份文章统计汇总记录">5月</li>
			<li title="${sysuser}的六月份文章统计汇总记录">6月</li>
			<li title="${sysuser}的七月份文章统计汇总记录">7月</li>
			<li title="${sysuser}的八月份文章统计汇总记录">8月</li>
			<li title="${sysuser}的九月份文章统计汇总记录">9月</li>
			<li title="${sysuser}的十月份文章统计汇总记录">10月</li>
			<li title="${sysuser}的十一月份文章统计汇总记录">11月</li>
			<li title="${sysuser}的十二月份文章统计汇总记录">12月</li>
		</ul>
	</div>
	</div>
	<div class="category">
	<div title="${sysuser}的标签统计记录汇总" class="title">${sysuser}的标签</div>
		<div class="category_content">
		<#list ftslable as f2>
			<a  title="${sysuser}的(${f2.name})标签内共有${f2.number}篇文章记录" href="${projname}${f2.link}">${f2.name}</a>
		</#list>
		</div>
	</div>
	<div class="category">
		<div class="title">友情连接</div>
		<div class="label">
			<#list links as lk>
				<a target="_blank" title="${sysuser}的小伙伴(${lk.name})的网站地址" href="${lk.path}">${lk.name}</a>
			</#list>
		</div>
	</div>
</div>