<div class="header">
<h1 class="title"><a title="${sysuser}的文章标题:${art.title}" href="${projname}/${writeurl}/${art.id}.html">${art.title}</a></h1>
<div class="meta">
	<span class="muted">
	<#list funs as fn><#if fn.funcode==art.cls><a title="${sysuser}的文章类型:${fn.name}" href="${projname}/html/list/${fn.funcode}.html"><i class="icon-list-alt"></i>${fn.name}</a></#if></#list>
	</span>				
	<time class="muted"><i class="icon-time"></i>${art.sendtime?substring(0,10)}</time>
	<span class="muted lables"><i class="icon-lables"></i>
	<#list labs as lab><#if lab.aid==art.id><a title="${sysuser}的文章标签:${lab.lid}" href="${projname}/html/labelgroup/${lab.lid}.html">${lab.lid}</a></#if></#list>
	</span>
</div>
</div>
<!-- 内容 -->
<div id="article" class="data-content">${art.atr_content}
<#if art.arttext??>${art.arttext}</#if>
<#if art.catalog??>
<dl id="catalog">
<dt class="index">目录</dt>
${art.catalog}</dl>
</#if>
</div>
<div class="sendall">
<div class="bdsharebuttonbox">
<a href="#" class="bds_qzone" data-cmd="qzone" title="分享${sysuser}的文章至QQ空间"></a>
<a href="#" class="bds_tsina" data-cmd="tsina" title="分享${sysuser}的文章至新浪微博"></a>
<a href="#" class="bds_tqq" data-cmd="tqq" title="分享${sysuser}的文章至腾讯微博"></a>
<a href="#" class="bds_renren" data-cmd="renren" title="分享${sysuser}的文章至人人网"></a>
<a href="#" class="bds_weixin" data-cmd="weixin" title="分享${sysuser}的文章至微信"></a>
<a href="#" class="bds_more" data-cmd="more" title="查看更多分享${sysuser}文章的方式" ></a></div>
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"1","bdSize":"16"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
</div>
<div class="article-page">
<span class="article-page-prev"><#if prev??>上一篇 <a title="${sysuser}的上一篇文章:${prev.title}" id="prev_page" href="${projname}/${writeurl}/${prev.id}.html">${prev.title}</a></#if></span>
<span class="article-page-next"><#if next??>下一篇 <a title="${sysuser}的下一篇文章:${next.title}" id="next_page" href="${projname}/${writeurl}/${next.id}.html">${next.title}</a></#if></span>
</div>
<div class="relates">
<h3>相关的文章</h3>
<ul>
<li><a href="#">暂未开启</a></li>
</ul>
<#if art.discuss?? && art.discuss=='true'>
<div class="ds-thread" data-thread-key="${art.id}" data-title="${art.title}" data-url="http://www.prdblog.com/${writeurl}/${art.id}.html"></div>
</#if>
</div>
<!--${crtime}-->