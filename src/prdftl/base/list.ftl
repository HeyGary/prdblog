<#list arts as at>
	<div class="article">
		<div class="list_header">
			<#list funs as fn>
				<#if fn.funcode==at.cls><a  title="${sysuser}的文章类型:${fn.name}" class="type" href="${projname}/html/list/${fn.funcode}.html">${fn.name}<i></i></a></#if>
			</#list>
			<h2><a title="${sysuser}的文章标题:${at.title}" href="${projname}/${pageurl}/${at.id}.html">${at.title}
			<#if at.top?? && at.top=='true'>[置顶]</#if>
			</a></h2>
		</div>
		<p class="base_list">本条发布于${at.sendtime?substring(0,10)},由   ${at.addusername} 发布
		,帖上<#list labs as lab><#if lab.aid==at.id><a title="${sysuser}的文章标签:${lab.lid}" href="${projname}/html/labelgroup/${lab.lid}.html">${lab.lid}</a></#if></#list>标签
		</p>
		<div class="note clearfix">${at.breviary}</div>
		<a title="查看${sysuser}的文章的全部内容" href="${projname}/${pageurl}/${at.id}.html" class="read-more">阅读全文...</a>
	</div>
</#list>
<div class="pagination">
    <#if thpage!=1><a title="${sysuser}的上一篇文章" id="prev_page" href="${projname}/${pageurl}<#if thpage!=2>${thpage-1}</#if>.html">上一页</a></#if>${pagea}<#if thpage!=sumpage><a title="${sysuser}的下一篇文章" id="next_page" href="${projname}/${pageurl}${thpage+1}.html">下一页</a></#if>
</div>
<!--${crtime}-->