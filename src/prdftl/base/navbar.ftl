<!--导航-->
<div id="navbar" class="navbar"><ul class="clearfix">
	<li class="title">${sysuser}</li>
	 <#list funs as fn>
	<#if fn.parentid==''>
	    	<#if fn.id=='index'>
	    		<li><a href="${projname}/html/index.html">${fn.name}</a>
	   	 	<#else>
			    <li><a 
			    <#if fn.ftltype=="1" ||  fn.ftltype=="2">href="${projname}/html/list/${fn.funcode}.html"</#if>
			    <#if fn.ftltype=="4">href="${fn.link}" target="_blank"</#if>
			    >${fn.name}</a>
	    </#if>
	    	<#assign isone = 1>
	    	<#list funs as f>
		        <#if f.parentid==fn.id>
		        	<#if isone==1><ul class="dropdown-menu"><#assign isone = 0></#if>
						<li><a
						<#if f.ftltype=="1" ||  f.ftltype=="2">href="${projname}/html/list/${f.funcode}.html"</#if>
			    		<#if f.ftltype=="4">href="${f.link}" target="_blank"</#if>						 
						 >${f.name}</a></li>
				</#if>
		   </#list>
		   <#if isone==0></ul></#if>
	    </li>
	</#if>
	</#list>
<!--
<li class="search"><form action=""><input type="text" placeholder="请输入搜索内容" class="input seach"/></form></li>
-->
</ul></div>