<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${sysuser} ${art.title}</title>
<#if art.seo??><meta name="description" content="${sysuser},${art.seo}"/></#if>
<#if art.keywords??><meta name="keywords" content="${art.keywords}"/></#if>
<#include '/loadbase/base.ftl'>
</head>
<body>
<#include '/loadbase/navbar.ftl'>
<!-- 内容 -->
<div class="main clearfix">
<div id="content" class="content">
<div id="article" class="alone">${art.atr_content}
<#if art.arttext??>${art.arttext}</#if>
<#if art.catalog??>
<dl id="catalog">${art.catalog}</dl>
</#if>
<#if art.discuss?? && art.discuss=='true'>
<div class="duoshuo_hr"></div>
<div class="ds-thread" data-thread-key="${art.id}" data-title="${art.title}" data-url="http://www.prdblog.com/${writeurl}/${art.id}.html"></div>
</#if>
</div>
</div>
<#include '/loadbase/right.ftl'>
</div>
<#include '/loadbase/floor.ftl'>
</body>
</html>