package org.porridge.prdblog.article.action;

import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import org.porridge.module.main.po.User;
import org.porridge.prdblog.article.business.ArticleBusiness;
import org.porridge.prdblog.article.business.FrameworkBusiness;
import org.porridge.prdblog.article.po.Article;
import org.porridge.util.BaseUtil;
import org.porridge.util.Constant;
import org.porridge.util.MysqlBackUtil;
import org.porridge.util.jqgrid.GridPO;
import org.porridge.util.jqgrid.JqgridUtil;
import org.porridge.util.qiniu.QiniuUtil;
import org.transmit.ActionContext;
import org.transmit.ActionUtil;
import org.transmit.form.vali.ValidationPage;
import org.transmit.key.Action;
import org.transmit.key.Business;
import org.transmit.key.Parameters;
import org.transmit.key.Results;
import org.transmit.key.ReturnJSON;
import org.transmit.key.ReturnText;
import org.transmit.util.FileUtil;

import com.qiniu.api.rsf.ListItem;

/**
 * 文章添加功能
* @Title: ArticleAction.java 
* @Package org.porridge.prdblog.article.action 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月21日 下午10:45:07 
* @version V1.0
 */
@Action("prd")
public class ArticleAction {
	@Business
	private ArticleBusiness artbusiness;
	@Business
	private FrameworkBusiness framebusiness;
	
	/**
	 * 七牛返回图片列表
	 * @return
	 */
	@ReturnText
	public String qinuiimages(){
		List<ListItem> list=QiniuUtil.getList(Constant.SETTING.getQiniu_name(),"images",20);
		StringBuffer stb=new StringBuffer();
		for(ListItem ls:list){
			stb.append(ls.key+"? imageView/1/w/790/q/100|watermark/2/text/QHBvcnJpZGdl/font/Y2FsaWJyaQ==/fontsize/500/fill/IzAwMzVGRg==/dissolve/100/gravity/SouthEast/dx/10/dy/10"+"ue_separate_ue");
		}
		return stb.toString();
	}
	
	/**
	 * 重新生成所有页面
	 * @return
	 * @throws Exception 
	 */
	@ReturnText
	public String RefreshCreateHtml() throws Exception{
		return String.valueOf(this.framebusiness.RefreshCreateHtml());
	}
	/**
	 * 查询表格
	 * @return
	 * @throws Exception
	 */
	@ReturnText
	public String artjqgrid() throws Exception{
		GridPO grid;
		String ret="";
			grid = JqgridUtil.getGridPath();
			if(grid!=null)
				ret=this.artbusiness.jqgrid(grid);
			return ret;
	}
	/**
	 * 置顶数据
	 * @return
	 * @throws Exception 
	 */
	@ReturnText
	public String topArticle() throws Exception{
		String id=ActionContext.getRequest().getParameter("id");
		String ret="null";
		if(id!=null && id.trim().length()>0){
			Article art=this.artbusiness.findPo(Article.class,id.trim());
			if("true".equals(art.getTop()))
				art.setTop("");
			else
				art.setTop("true");
			ret=String.valueOf(this.artbusiness.updatePo(art)==0?"false":"true");
		}
		return ret;
	}
	/**
	 * 查询单条文章数据
	 * @return
	 * @throws Exception 
	 */
	@ReturnJSON
	public Article findArticle() throws Exception{
		Article art=new Article();
		String id=ActionContext.getRequest().getParameter("id");
		if(id!=null && id.trim().length()>0){
			art=this.artbusiness.findPo(Article.class,id.toString().trim());
		}
		return art;
	}
	/**
	 * 通过code查询其是否有单条数据
	 * @return
	 * @throws Exception
	 */
	@ReturnJSON
	public Article findArticleCode() throws Exception{
		Article art=null;
		String code=ActionContext.getRequest().getParameter("code");
		if(code!=null && code.trim().length()>0){
			art=this.artbusiness.findArticleCode(code);
		}
		return art;
	}
	/**
	 * 删除数据
	 * @return
	 * @throws Exception 
	 */
	@ReturnText
	public String delArticle() throws Exception{
		String id=ActionContext.getRequest().getParameter("id");
		boolean bl=false;
		if(id!=null && id.trim().length()>0){
			bl=this.artbusiness.delArticle(id.trim());
		}
		return String.valueOf(bl);
	}
	/**
	 * 保存文章
	 * 待处理完主要几点:
	 * 1保存文章
	 * 2保存他的附件 比如图片
	 * 3保存他定义的标签
	 * @throws Exception 
	 */
	public void saveArticle() throws Exception{
		Article art=ActionUtil.find(Article.class);
		User user=(User)ActionContext.getSession().getAttribute("user");
		if(ValidationPage.ValidationPO(art)){
			 PrintWriter out=ActionContext.getPrintWriter();
			 art.setId(null);
			 art.setType("0");
			 art.setSendtime(BaseUtil.convertDate(new Date()));
			 out.print(artbusiness.saveArticle(art,user));
		}
	}
	/**
	 * 修改文章
	 * @throws Exception
	 */
	public void updateArticle() throws Exception{
		Article art=ActionUtil.find(Article.class);
		if(ValidationPage.ValidationPO(art)){
			 PrintWriter out=ActionContext.getPrintWriter();
			 art.setType(null);
			 art.setSendtime(null);
			 if(artbusiness.updateArticle(art))
				 out.print("{\"result\":true}");
			 else
				 out.print("{\"result\":false,\"name\":null,\"text\":\"保文章失败\"}");
		}
	}
}
