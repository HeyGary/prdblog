package org.porridge.prdblog.article.po;

import org.db.comments.Key;
import org.db.comments.Table;

/**
 * 附件表
* @Title: Accessory.java 
* @Package org.porridge.prdblog.article.po 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月21日 下午10:31:42 
* @version V1.0
 */
@Table(name="prd_accessory")
public class Accessory {
	@Key
	private String id;
	private String aid;//文章id
	private String hash;//七牛的hash
	private String address;//地址  访问地址前加上  http://porridge.qiniudn.com/   是可配置的哦.
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAid() {
		return aid;
	}
	public void setAid(String aid) {
		this.aid = aid;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
