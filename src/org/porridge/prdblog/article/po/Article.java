package org.porridge.prdblog.article.po;

import org.db.comments.Key;
import org.db.comments.Table;
import org.transmit.form.key.Vali;
import org.transmit.form.key.Verify;

/**
 * 文章
* @Title: Article.java 
* @Package org.porridge.prdblog.article.po 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月21日 下午10:25:57 
* @version V1.0
 */
@Table(name="prd_article")
public class Article {
	@Key
	private String id;
	private String tid;//自定义id如果有定义id则优先测试自定义id是否可以正常使用
	@Vali({@Verify(name = "empty", reason = "文章不能为空"),
		   @Verify(name = "lenmax", reason = "系统名称长度不能超过100",len=100)})
	private String title;//文章标题
	@Vali({@Verify(name = "empty", reason = "文章不能为空")})
	private String atr_content;//文章内容
	@Vali({@Verify(name = "empty", reason = "文章类型不能为空"),
		   @Verify(name = "lenmax", reason = "文章长度不能超过100",len=100)})
	private String cls;//文章类型
	private String type;//文章发布方式  0网站发布  1通过email的方法发送的
	@Vali({@Verify(name = "lenmax", reason = "seo不能超过1000",len=1000)})
	private String seo;//seo关键字
	private String sendtime;//发布时间
	@Vali({@Verify(name = "lenmax", reason = "附件数据不能超过3000",len=3000)})
	private String accid;//附件数据  hash-address
	@Vali({@Verify(name = "lenmax", reason = "标签长度不能超过3000",len=3000)})
	private String lablename;//标签名称
	private String labletype;//文章类型  0单条   1集合
	private String adduserid;//添加人ID
	private String addusername;//添加人
	private String breviary;//文章缩略
	private String catalog;//文章目录
	private String top;//置顶  如果置顶则为true
	private String keywords;//文章描述
	private String discuss;//开启评论
	private String arttext;//文章附带数据.自定义html代码
	
	public String getArttext() {
		return arttext;
	}
	public void setArttext(String arttext) {
		this.arttext = arttext;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getDiscuss() {
		return discuss;
	}
	public void setDiscuss(String discuss) {
		this.discuss = discuss;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getTop() {
		return top;
	}
	public void setTop(String top) {
		this.top = top;
	}
	public String getCatalog() {
		return catalog;
	}
	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}
	public String getBreviary() {
		return breviary;
	}
	public void setBreviary(String breviary) {
		this.breviary = breviary;
	}
	public String getAdduserid() {
		return adduserid;
	}
	public void setAdduserid(String adduserid) {
		this.adduserid = adduserid;
	}
	public String getAddusername() {
		return addusername;
	}
	public void setAddusername(String addusername) {
		this.addusername = addusername;
	}
	public String getLabletype() {
		return labletype;
	}
	public void setLabletype(String labletype) {
		this.labletype = labletype;
	}
	public String getLablename() {
		return lablename;
	}
	public void setLablename(String lablename) {
		this.lablename = lablename;
	}
	public String getAccid() {
		return accid;
	}
	public void setAccid(String accid) {
		this.accid = accid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAtr_content() {
		return atr_content;
	}
	public void setAtr_content(String atr_content) {
		this.atr_content = atr_content;
	}
	public String getCls() {
		return cls;
	}
	public void setCls(String cls) {
		this.cls = cls;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getSeo() {
		return seo;
	}
	public void setSeo(String seo) {
		this.seo = seo;
	}
	public String getSendtime() {
		return sendtime;
	}
	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}
}
