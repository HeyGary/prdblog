package org.porridge.prdblog.article.po;

/**
 * 临时存放数据的PO.不与数据库相关联
* @Title: FtlPo.java 
* @Package org.porridge.prdblog.article.po 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月29日 下午10:50:29 
* @version V1.0
 */
public class FtlPo {
	private String link;
	private String number;
	private String name;
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
