package org.porridge.prdblog.link.business;

import org.db.createsql.AbstrDao;
import org.porridge.prdblog.label.dao.LabelDao;
import org.porridge.prdblog.link.dao.LinkDao;
import org.porridge.prdblog.link.po.Link;
import org.porridge.util.jqgrid.GridPO;

/**
 * 友情连接
* @Title: LinkBusiness.java 
* @Package org.porridge.prdblog.link.business 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年5月5日 下午9:15:58 
* @version V1.0
 */
public class LinkBusiness extends AbstrDao<LinkDao>{
	/**
	 * 查询link jqgrid
	 * @param grph
	 * @return
	 * @throws Exception
	 */
	public String jqgrid(GridPO grph) throws Exception{
		String ret="";
		if(grph!=null){
			ret=this.absDao.getListJqgrid(Link.class,grph.getFilename(),grph.getWhere(),grph.getSidx(),grph.getSord(),grph.getPage(),grph.getRows());
		}
		return ret;
	}
}
