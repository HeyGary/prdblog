package org.porridge.prdblog.link.po;

import org.db.comments.Key;
import org.db.comments.Table;
import org.transmit.form.key.Vali;
import org.transmit.form.key.Verify;

/**
 * 友情连接
* @Title: Link.java 
* @Package org.porridge.prdblog.link.po 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年5月5日 下午8:59:25 
* @version V1.0
 */
@Table(name="prd_link")
public class Link {
	@Key
	private String id;
	@Vali({@Verify(name = "empty", reason = "友连名称不能为空"),
		   @Verify(name = "lenmax", reason = "标签长度不能超过300",len=300)})
	private String name;
	@Vali({@Verify(name = "empty", reason = "友连地址不能为空"),
		   @Verify(name = "lenmax", reason = "标签长度不能超过500",len=500)})
	private String path;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
