package org.porridge.prdblog.link.action;

import java.io.PrintWriter;

import org.porridge.prdblog.label.po.Label;
import org.porridge.prdblog.link.business.LinkBusiness;
import org.porridge.prdblog.link.po.Link;
import org.porridge.util.jqgrid.GridPO;
import org.porridge.util.jqgrid.JqgridUtil;
import org.transmit.ActionContext;
import org.transmit.ActionUtil;
import org.transmit.form.vali.ValidationPage;
import org.transmit.key.Action;
import org.transmit.key.Business;
import org.transmit.key.ReturnJSON;
import org.transmit.key.ReturnText;

/**
 * 友情连接
* @Title: LinkAction.java 
* @Package org.porridge.prdblog.link.action 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年5月5日 下午9:16:43 
* @version V1.0
 */
@Action("prd")
public class LinkAction {
	@Business
	private LinkBusiness linkbusiness;
	
	/**
	 * 查询表格
	 * @return
	 * @throws Exception
	 */
	@ReturnText
	public String linkjqgrid() throws Exception{
		GridPO grid;
		String ret="";
		grid = JqgridUtil.getGridPath();
		if(grid!=null)
			ret=this.linkbusiness.jqgrid(grid);
		return ret;
	}
	
	/**
	 * 保存友情连接
	 */
	public void svaelink(){
		Link link=ActionUtil.find(Link.class);
		if(ValidationPage.ValidationPO(link)){
			 PrintWriter out=ActionContext.getPrintWriter();
			try {
				link.setId(null);
				if(linkbusiness.savePo(link)!=null){
					out.print("{\"result\":true}");
				}else{
					out.print("{\"result\":false,\"name\":\"name\",\"text\":\"保存失败\"}");
				}
			} catch (Exception e) {
				out.print("{\"result\":false,\"name\":\"name\",\"text\":\"保存失败,系统发生错误\"}");
				e.printStackTrace();
			}
		}
	}
	/**
	 * 修改连接
	 * @throws Exception
	 */
	public void updatelink() throws Exception{
		Link link=ActionUtil.find(Link.class);
		 PrintWriter out=ActionContext.getPrintWriter();
		 String ret="{\"result\":false,\"name\":\"name\",\"text\":\"修改友连失败,数据为空.\"}";
		 
		 if(link!=null && link.getId()!=null && link.getName()!=null && link.getPath()!=null){
			if(this.linkbusiness.updatePo(link)!=0)
				 ret="{\"result\":true}";
		 }
		 
		 out.print(ret);
	}
	/**
	 * 查询单条友连
	 * @return
	 * @throws Exception
	 */
	@ReturnJSON
	public Link findlink() throws Exception{
		Object obj=ActionContext.getRequest().getParameter("id");
		Link link=new Link();
		if(obj!=null && obj.toString().trim().length()>0)
			link=this.linkbusiness.findPo(Link.class,obj.toString().trim());
		return link;
	}
	/**
	 * 删除友情连接
	 * @return
	 * @throws Exception
	 */
	@ReturnText
	public String dellink() throws Exception{
		Link link=ActionUtil.find(Link.class);
		boolean bl=false;
		if(link!=null && link.getId()!=null)
			bl=this.linkbusiness.delPo(link)>0?true:false;
		return String.valueOf(bl);
	}
}
