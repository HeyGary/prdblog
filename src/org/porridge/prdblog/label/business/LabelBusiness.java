package org.porridge.prdblog.label.business;

import java.util.List;

import org.db.createsql.AbstrDao;
import org.porridge.module.main.po.User;
import org.porridge.prdblog.label.dao.LabelDao;
import org.porridge.prdblog.label.po.Label;
import org.porridge.util.jqgrid.GridPO;

/**
 * 标签逻辑层
* @Title: LabelBusiness.java 
* @Package org.porridge.prdblog.label.business 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月22日 下午8:35:39 
* @version V1.0
 */
public class LabelBusiness extends AbstrDao<LabelDao>{
	/**
	 * 查询名称是否存在
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public int findCount(String where) throws Exception{
		return this.absDao.getCount(Label.class,where);
	}
	/**
	 * 查询标签表格
	 * @param grph
	 * @return
	 * @throws Exception
	 */
	public String jqgrid(GridPO grph) throws Exception{
		String ret="";
		if(grph!=null){
			ret=this.absDao.getListJqgrid("v_label",grph.getFilename(),grph.getWhere(),grph.getSidx(),grph.getSord(),grph.getPage(),grph.getRows());
		}
		return ret;
	}
	/**
	 * 自动补全查询  只返回十条数据
	 * @param where
	 * @return
	 * @throws Exception 
	 */
	public String findLables(String where) throws Exception{
		List<Label> lebs=this.absDao.getList(Label.class,"name",where,"name","desc",0,10);
		StringBuffer stb=new StringBuffer();
		for(Label l:lebs)
			stb.append("\""+l.getName()+"\",");
		if(lebs.size()>0)
			return "["+stb.substring(0,stb.length()-1)+"]";
		else
			return "[]";
		
	}
}
