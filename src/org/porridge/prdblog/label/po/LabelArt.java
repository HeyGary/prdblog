package org.porridge.prdblog.label.po;

import org.db.comments.Key;
import org.db.comments.Table;

/**
 * 标签关联
* @Title: LabelArt.java 
* @Package org.porridge.prdblog.label.po 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月22日 下午10:33:50 
* @version V1.0
 */
@Table(name="prd_labelart")
public class LabelArt {
	@Key
	private String id;
	private String aid;//文章id
	private String lid;//标签id
	private String addtime;//添加时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAid() {
		return aid;
	}
	public void setAid(String aid) {
		this.aid = aid;
	}
	public String getLid() {
		return lid;
	}
	public void setLid(String lid) {
		this.lid = lid;
	}
	public String getAddtime() {
		return addtime;
	}
	public void setAddtime(String addtime) {
		this.addtime = addtime;
	}
}
