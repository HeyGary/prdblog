package org.porridge.prdblog.label.po;

import org.db.comments.Key;
import org.db.comments.Table;
import org.transmit.form.key.Vali;
import org.transmit.form.key.Verify;

/**
 * 标签
* @Title: Label.java 
* @Package org.porridge.prdblog.label.po 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月22日 下午8:26:40 
* @version V1.0
 */
@Table(name="prd_label")
public class Label {
	@Key
	private String id;
	@Vali({@Verify(name = "empty", reason = "标签名称不能为空"),
		   @Verify(name = "lenmax", reason = "标签长度不能超过100",len=100)})
	private String name;//标签名称
	private String addtime;//添加时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddtime() {
		return addtime;
	}
	public void setAddtime(String addtime) {
		this.addtime = addtime;
	}
}
