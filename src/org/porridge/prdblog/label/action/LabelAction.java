package org.porridge.prdblog.label.action;

import java.io.PrintWriter;
import java.util.Date;

import org.porridge.module.main.po.User;
import org.porridge.prdblog.label.business.LabelBusiness;
import org.porridge.prdblog.label.po.Label;
import org.porridge.util.BaseUtil;
import org.porridge.util.jqgrid.GridPO;
import org.porridge.util.jqgrid.JqgridUtil;
import org.transmit.ActionContext;
import org.transmit.ActionUtil;
import org.transmit.form.key.Vali;
import org.transmit.form.vali.ValidationPage;
import org.transmit.key.Action;
import org.transmit.key.Business;
import org.transmit.key.ReturnJSON;
import org.transmit.key.ReturnText;

/**
 * 标签
* @Title: LabelAction.java 
* @Package org.porridge.prdblog.label.action 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月22日 下午8:30:53 
* @version V1.0
 */
@Action("prd")
public class LabelAction {
	@Business
	private LabelBusiness labelbusiness;
	
	/**
	 * 保存标签名称
	 * @throws Exception
	 */
	public void saveLabel() throws Exception{
		Label label=ActionUtil.find(Label.class);
		if(ValidationPage.ValidationPO(label)){
			 PrintWriter out=ActionContext.getPrintWriter();
			 if(labelbusiness.findCount("name='"+label.getName().trim()+"'")==0){
				 label.setId(null);
				 label.setAddtime(BaseUtil.convertDate(new Date(),"yyyy-MM-dd"));
				 label.setName(label.getName().trim());
				 if(this.labelbusiness.savePo(label)!=null){
					 out.print("{\"result\":true}");
				 }else{
					 out.print("{\"result\":false,\"name\":\"name\",\"text\":\"保存失败\"}");
				 }
			 }else{
				 out.print("{\"result\":false,\"name\":\"name\",\"text\":\"标签名称已存在\"}");
			 }
		}
	}
	/**
	 * 查询表格出来
	 * @return
	 * @throws Exception
	 */
	@ReturnText
	public String labeljqgrid() throws Exception{
		GridPO grid;
		String ret="";
		grid = JqgridUtil.getGridPath();
		if(grid!=null)
			ret=this.labelbusiness.jqgrid(grid);
		return ret;
	}
	/**
	 * 查询单条标签
	 * @return
	 * @throws Exception
	 */
	@ReturnJSON
	public Label findLabel() throws Exception{
		Object obj=ActionContext.getRequest().getParameter("id");
		Label label=new Label();
		if(obj!=null && obj.toString().trim().length()>0)
			label=this.labelbusiness.findPo(Label.class,obj.toString().trim());
		return label;
	}
	/**
	 * 删除标签
	 * @return
	 * @throws Exception
	 */
	@ReturnText
	public String delLabel() throws Exception{
		Label label=ActionUtil.find(Label.class);
		boolean bl=false;
		if(label!=null && label.getId()!=null)
			bl=this.labelbusiness.delPo(label)>0?true:false;
		return String.valueOf(bl);
	}
	/**
	 * 修改标签
	 * @throws Exception 
	 */
	public void updateLabel() throws Exception{
		Label label=ActionUtil.find(Label.class);
		 PrintWriter out=ActionContext.getPrintWriter();
		 String ret="{\"result\":false,\"name\":\"name\",\"text\":\"修改标签失败,数据为空.\"}";
		 
		 if(label!=null && label.getId()!=null && label.getName()!=null){
			 if(this.labelbusiness.findCount("name='"+label.getName()+"' and id!='"+label.getId()+"'")==0){
				 if(this.labelbusiness.updatePo(label)!=0)
					 ret="{\"result\":true}";
			 }else{
				 ret="{\"result\":false,\"name\":\"name\",\"text\":\"标签名已存在\"}";
			 }
		 }
		 out.print(ret);
	}
	/**
	 * 查询自动补全
	 * @return
	 * @throws Exception 
	 */
	@ReturnText
	public String findLables() throws Exception{
		Object obj=ActionContext.getRequest().getParameter("keyword");
		String ret="[]";
		if(obj!=null && obj.toString().trim().length()>0){
			ret=this.labelbusiness.findLables("name like '"+obj.toString().trim()+"%'");
		}
		return ret;
	}
}
