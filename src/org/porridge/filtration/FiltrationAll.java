package org.porridge.filtration;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Timer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.db.createdao.BaseDao;
import org.porridge.module.main.po.User;
import org.porridge.util.BaseUtil;
import org.porridge.util.Constant;
import org.porridge.util.time.ArticleTime;
import org.transmit.util.FileUtil;

/**
 * 用户登录数据过滤
* @Title: FiltrationAll.java 
* @Package org.porridge.filtration 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月16日 下午7:42:53 
* @version V1.0
 */
public class FiltrationAll  implements Filter{
	final Logger logger = Logger.getLogger(this.getClass());
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain fiter) throws IOException, ServletException {
		Date beginDate = new Date();
		HttpServletRequest reqs = (HttpServletRequest) request;
		String path=reqs.getServletPath();
		HttpServletResponse resp=(HttpServletResponse)response;
		if(Constant.PROJECTPATH==null)
			Constant.PROJECTPATH=reqs.getContextPath();
		if(valipage(path)){
			String pjax=reqs.getParameter("pjax");
			if("true".equals(pjax) && path.lastIndexOf(".html")+5==path.length()){//如果是pjax请求由直接返回相对应的页面
				reqs.getRequestDispatcher(path.replace(".html","_pjax.html")).forward(reqs,resp);
			}else
				fiter.doFilter(request, response);
		}else{
			User user=(User) reqs.getSession().getAttribute("user");
			if(user!=null)
				fiter.doFilter(request, response);
			else{
				if(path.indexOf("/pages")==0 || path.indexOf("/resource")==0)
					reqs.getRequestDispatcher("/pages/login.jsp").forward(reqs,resp);
				else
					fiter.doFilter(request, response);
			}
		}
		
		HttpServletRequest req = (HttpServletRequest)request;
		StringBuffer stb = new StringBuffer();
		Enumeration enu=request.getParameterNames();  
		while(enu.hasMoreElements()){  
			String key = (String)enu.nextElement();  
			stb.append(key + "=" + request.getParameter(key) + "&");
		}
		logger.info(req.getMethod() + " URL " + req.getServletPath() + "?" + stb.toString() + "执行时间：" + (new Date().getTime() - beginDate.getTime()) + "毫秒");
	}
	/**
	 * 验证页面
	 * @return
	 */
	private boolean valipage(String path){
		boolean bl=false;
		if(path.indexOf("/pages/login.jsp")!=-1)
			bl=true;
		else if(path.indexOf("/resource/blue")==0)
			bl=true;
		else if(path.indexOf("/resource/js/")==0)
			bl=true;
		else if(path.indexOf("/html")==0)
			bl=true;
		else if(path.indexOf("/resource/images/")==0)
			bl=true;
		else if("/module/user_userLogin.action".equals(path))
			bl=true;
		else if("/module/user_getimgcode.action".equals(path))
			bl=true;
		else if("/module/user_logout.action".equals(path))
			bl=true;
		return bl;
	}
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		System.out.println("读取配置文件...");
		Constant.SETTING=BaseUtil.findSetting();
		Timer time=new Timer("addEmail");
		//十分钟后启动该定时器,一个小时调用一次
		time.schedule(new ArticleTime(),10*60*1000,60*60*1000);
	}
	@Override
	public void destroy() {
	}
}
