package org.porridge.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * 
 * <p>Title: ImgCodeUtil   </p>
 * <p>Description:验证码生成类 </p>
 * <p>Company: web</p>
 * <p>Create Date/Time: 2013年12月15日 下午3:54:40<p>
 * @version 1.0
 *
 */
public class ImgCodeUtil {
	
	public static int randomsize=4;//验证码位数
	
	public static Map<String,String> map=new HashMap<String, String>();
	
	/**
	 * 校验验证码是否正常
	 * @param code
	 * @return
	 */
	public static boolean valiimgcode(String code,String sessionid){
		boolean bl=false;
		if(map.get(sessionid)!=null && code!=null && code.length()==4){
			if(map.get(sessionid).toLowerCase().equals(code.toLowerCase()))
				bl=true;
		}
		return bl;
	}
	
	public static void removemapkey(String key){
		if(key!=null && key.trim().length()>0)
			map.remove(key);
	}
	/**
	 * 生成验证码并且输出放在全局静态变量里边去
	 * @param response
	 * @param sessionid
	 * @return
	 * @throws Exception
	 */
	public static boolean setCodeImages(HttpServletResponse response,String sessionid) throws Exception {
		try {
			int width = 65;
			int height = 27;
			// 取得一个4位随机字母数字字符串
			String code = RandomStringUtils.random(randomsize,true,true);
			map.put(sessionid,code);
			// 保存入session,用于与用户的输入进行比较.
			response.setContentType("images/jpeg");
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			ServletOutputStream out = response.getOutputStream();
			BufferedImage image = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
			Graphics g = image.getGraphics();
			// 设定背景色
			g.setColor(getRandColor(200, 250));
			g.fillRect(0, 0, width, height);
			// 设定字体
			Font mFont = new Font("Times New Roman", Font.BOLD, 22);// 设置字体
			g.setFont(mFont);
			// 画边框
			// g.setColor(Color.BLACK);
			// g.drawRect(0, 0, width - 1, height - 1);
			// 随机产生干扰线，使图象中的认证码不易被其它程序探测到
			g.setColor(getRandColor(150, 200));
			// 生成随机类
			Random random = new Random();
			for (int i = 0; i < 155; i++) {
				int x2 = random.nextInt(width);
				int y2 = random.nextInt(height);
				int x3 = random.nextInt(12);
				int y3 = random.nextInt(12);
				g.drawLine(x2, y2, x2 + x3, y2 + y3);
			}
			// 将认证码显示到图象中
			g.setColor(new Color(20 + random.nextInt(110), 20+random.nextInt(110), 20 + random.nextInt(110)));
			g.drawString(code, 2, 22);
			// 图象生效
			g.dispose();
			// 输出图象到页面
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(response.getOutputStream()); 
			encoder.encode(image);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			map.remove(sessionid);
			return false;
		}
		return true;
	}
	
	private static Color getRandColor(int fc, int bc) { // 给定范围获得随机颜色
		Random random = new Random();
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);
		return new Color(r, g, b);
	}
}
