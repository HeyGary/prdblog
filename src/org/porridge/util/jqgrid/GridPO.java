package org.porridge.util.jqgrid;

/**
 * jqgrid表格分页封装
* @Title: GridPath.java 
* @Package org.porridge.util.jqgrid 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月16日 下午9:02:10 
* @version V1.0
 */
public class GridPO {
	private String id;//操作的主键内容
	private String oper;//提交类型
	private String searchField;//提交字段名称
	//bw LIKE val%  eq  =  ne !=  lt  < le <=  gt >  ge >= ew LIKE %val  cn LIKE %val% 
	private String searchOper;//查询类型
	private String searchString;//查询内容
	private String filename;//列名
	private String sidx;//排序字段
	private String sord;//排序类型
	private int page;//请求目标页数
	private int rows;//每页数量
	
	private String fixedwhere;//固定条件.一般是第一次就传入的条件
	private String where;//可变动条件
	
	public String getWhere() {
		String retwhere="";
		if(!(this.searchField!=null && this.searchField.trim().length()>0)){//自己查询的方式
			if(this.where!=null && this.where.trim().length()>0)
				retwhere=this.where;
			else
				retwhere="1=1";
		}else{//插件自带查询类型
			if(this.searchString==null)
				this.searchString="";
			
			this.searchString=this.searchString.trim();
			if(this.searchString.length()==0)
				retwhere="1=1";
			if("bw".equals(this.searchOper)){//开始于
				retwhere=this.searchField+" like '"+this.searchString+"%' ";
			}else if("bn".equals(this.searchOper)){//不开始于
				retwhere=this.searchField+" not like '"+this.searchString+"%' ";
			}else if("eq".equals(this.searchOper)){//等于 
				retwhere=this.searchField+"='"+this.searchString+"' ";
			}else if("ne".equals(this.searchOper)){//不等于 
				retwhere=this.searchField+"!='"+this.searchString+"' ";
			}else if("lt".equals(this.searchOper)){
				retwhere=this.searchField+"<'"+this.searchString+"' ";
			}else if("le".equals(this.searchOper)){
				retwhere=this.searchField+"<='"+this.searchString+"' ";
			}else if("gt".equals(this.searchOper)){
				retwhere=this.searchField+">'"+this.searchString+"' ";
			}else if("ge".equals(this.searchOper)){
				retwhere=this.searchField+">='"+this.searchString+"' ";
			}else if("ew".equals(this.searchOper)){//结束于
				retwhere=this.searchField+" like '%"+this.searchString+"' ";
			}else if("en".equals(this.searchOper)){//不结束于
				retwhere=this.searchField+" not like '%"+this.searchString+"' ";
			}else if("cn".equals(this.searchOper)){//包含
				retwhere=this.searchField+" like '%"+this.searchString+"%' ";
			}else if("on".equals(this.searchOper)){
				retwhere=this.searchField+" in ('"+this.searchString+"') ";
			}else if("nc".equals(this.searchOper)){
				retwhere=this.searchField+" not in ('"+this.searchString+"') ";
			}else{
				retwhere="1=1";
			}
		}
		if(fixedwhere!=null && fixedwhere.trim().length()>0)
			retwhere="("+fixedwhere+") and ("+retwhere+")";
		
		//过滤四组坏蛋
		if(retwhere.indexOf("inert")!=-1 && retwhere.indexOf("into")!=-1)
			retwhere=retwhere.replaceAll("inert","").replaceAll("into","");
		if(retwhere.indexOf("drop")!=-1 && retwhere.indexOf("table")!=-1)
			retwhere=retwhere.replaceAll("drop","").replaceAll("table","");
		if(retwhere.indexOf("drop")!=-1 && retwhere.indexOf("view")!=-1)
			retwhere=retwhere.replaceAll("drop","").replaceAll("view","");
		if(retwhere.indexOf("drop")!=-1 && retwhere.indexOf("database")!=-1)
			retwhere=retwhere.replaceAll("drop","").replaceAll("database","");
		return retwhere;
	}
	
	public String getFixedwhere() {
		return fixedwhere;
	}
	public void setFixedwhere(String fixedwhere) {
		this.fixedwhere = fixedwhere;
	}

	public String getSearchField() {
		return searchField;
	}
	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}
	public String getSearchOper() {
		return searchOper;
	}
	public void setSearchOper(String searchOper) {
		this.searchOper = searchOper;
	}
	public String getSearchString() {
		return searchString;
	}
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOper() {
		return oper;
	}
	public void setOper(String oper) {
		this.oper = oper;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public void setWhere(String where) {
		this.where = where;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		if(rows>100)//不能让他请求过多资源
			rows=100;
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
}
