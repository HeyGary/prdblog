package org.porridge.util.jqgrid;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.transmit.ActionContext;


/**
 * jqgrid 工具类
* @Title: JqgridUtil.java 
* @Package org.porridge.util.jqgrid 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月16日 下午9:01:31 
* @version V1.0
 */
public class JqgridUtil {
	public static  GridPO getGridPath() throws IllegalAccessException, InvocationTargetException{
    	Map params =ActionContext.getRequest().getParameterMap();
     	GridPO grph=new GridPO();
     	BeanUtils.populate(grph,params);
     	return grph;
     }
}
