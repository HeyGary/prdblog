package org.porridge.util;

import org.porridge.module.util.po.Setting;

/**
 * 存放常量处
* @Title: Constant.java 
* @Package org.porridge.util 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月21日 下午9:54:57 
* @version V1.0
 */
public class Constant {
	/**
	 * 系统配置
	 */
	public static Setting SETTING=null;
	/**
	 * 项目地址
	 */
	public static String PROJECTPATH=null;
}
