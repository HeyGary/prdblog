package org.porridge.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.db.createdao.BaseDao;
import org.transmit.ActionContext;
import org.transmit.util.FileUtil;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

/**
 * 静态页面生成工具类
* @Title: FrameworkUtil.java 
* @Package org.porridge.util 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月26日 下午2:38:01 
* @version V1.0
 */
public class FrameworkUtil {
	public  final static Logger log = Logger.getLogger(FrameworkUtil.class);
	public static String projname=ActionContext.getRequest().getContextPath();
	private static String PATH=null;
	/**
	 * 先删除生成的静态文件.再重新生成
	 */
	public static void deleHtml(){
		setPath();
		File file=new File(PATH+"html");
		if(file.exists()){
			File[] fs=file.listFiles();
			for(File f:fs){
				if(!"resource".equals(f.getName())){
					FileUtil.deleFile(f.getPath());
				}
			}
		}else
			file.mkdirs();
	}
	
	private static void setPath(){
		if(PATH==null){
			String url=FileUtil.URL;
			if(url.indexOf("classes")!=-1)
				url=url.substring(0,url.indexOf("classes")-8);
			PATH=url;
		}
	}
	public static boolean createFtl(Map<String,Object> maps,String tflname,String fileurl){
		boolean bl=true;
		Writer out = null;
		try {
			setPath();
			if(maps==null)
				maps=new HashMap<String, Object>();
			maps.put("projname",projname);
			maps.put("sysuser",Constant.SETTING.getSys_user());
			maps.put("sysseo",Constant.SETTING.getSys_seo());
			maps.put("crtime",BaseUtil.convertDate(new Date()));
			//通过Freemaker的Configuration读取相应的ftl
			Configuration cfg = new Configuration();
			//设定去哪里读取相应的ftl模板文件
			String ftlurl=PATH+"WEB-INF"+File.separatorChar+"classes"+File.separator+"prdftl"+File.separatorChar;
			cfg.setDirectoryForTemplateLoading(new File(ftlurl));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			//设置读取编码
			cfg.setEncoding(Locale.CHINA, "UTF-8");
			cfg.setDefaultEncoding("UTF-8");
			//在模板文件目录中找到名称为name的文件
			Template template = cfg.getTemplate(tflname);
			//指定模板编码编码和生成文件输出流的编码
			template.setEncoding("UTF-8");
			fileurl=PATH+fileurl;
			
			File fs=new File(fileurl).getParentFile();
			if(!fs.exists())
				fs.mkdirs();
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileurl),"UTF-8"));//生成文件的目录
			//生成文件
			template.process(maps,out);
			//关闭资源
			cfg.clone();
			out.flush();
			out.close();
			log.info("成功生成文件："+fileurl);
		} catch (Exception e) {
			bl=false;
			e.printStackTrace();
		}
		return bl;
	}
}
