package org.porridge.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Date;

import org.porridge.util.qiniu.QiniuUtil;
import org.transmit.StartUtil;
import org.transmit.util.FileUtil;
/**
* @Title: MysqlBackUtil.java 
* @Package org.porridge.util 
* @Description: mysql备份,逢双备份,上传至七牛. 新开一个线程来执行备份数据
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年6月20日 下午9:24:37 
* @version V1.0
 */
public class MysqlBackUtil extends Thread{
	
	public void run() {
		String url=backdb();//备份数据库返回地址
		if(url!=null){
			uploadQiniu(url);
		}
	}
	/**
	 * 上传至七牛空间
	 * @param url
	 */
	public void uploadQiniu(String url){
		File file=new File(url);
		if(file.exists()){
			QiniuUtil.uploadFile("th-zhou","sqlback//"+file.getName(),url);
		}
	} 
	
	/**
	 * 备份数据库
	 * 返回生成地址
	 * @return
	 */
	public String backdb(){
		String dataname=StartUtil.findDatabaseConfig().getDatabase();
		if(dataname.indexOf("?")!=-1)
			dataname=dataname.substring(0,dataname.indexOf("?"));
		String url=FileUtil.URL+"mysqlBack";
		File file=new File(url);
		if(!file.exists())file.mkdirs();
		url+=File.separatorChar+dataname+BaseUtil.convertDate(new Date(),"yyyy_MM_dd_HH_mm_ss")+".sql";
		
		String mysqladdress=StartUtil.findConfig("mysqladdress")+"mysqldump ";
		
		String backsql=mysqladdress+" -u"+StartUtil.findDatabaseConfig().getUserName()+" -p"+StartUtil.findDatabaseConfig().getPassword();
		backsql+=" -h"+StartUtil.findDatabaseConfig().getUrl()+" "+dataname+">"+url;
		Process proc;
		try {
			System.out.println(backsql);
			proc = Runtime.getRuntime().exec("cmd /k " + backsql);
			proc.destroy();
			File filesql=new File(url);
			if(!filesql.isFile()){
				url=null;
			}
		} catch (Exception e) {
			url=null;
			System.out.println("备份数据库失败.");
			e.printStackTrace();
		}finally{
			return url;
		}
	}
}
