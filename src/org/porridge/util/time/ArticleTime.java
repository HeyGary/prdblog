package org.porridge.util.time;

import java.util.Date;
import java.util.TimerTask;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.porridge.util.BaseUtil;
import org.porridge.util.Constant;
import org.porridge.util.EmailUitl;
import org.porridge.util.MysqlBackUtil;

/**
 * 定时器调用方法
* @Title: ArticleTime.java 
* @Package org.porridge.util.time 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年5月18日 下午10:29:43 
* @version V1.0
 */
public class ArticleTime extends TimerTask{

	@Override
	public void run() {
		if("true".equals(Constant.SETTING.getEmail_isopen())){
			new EmailUitl(1,null).start();
		}
		//是双号以及凌晨二点的时候备份数据库并且上传至七牛保存
		boolean day=Integer.valueOf(BaseUtil.convertDate(new Date(),"dd"))%2==0?true:false;
		boolean time=BaseUtil.convertDate(new Date(),"hh").equals("02")?true:false;
		if(day && time)
			new MysqlBackUtil().start();
	}

}
