package org.porridge.module.util.po;

import org.transmit.form.key.Vali;
import org.transmit.form.key.Verify;

/**
 * 系统主要配置实体类
* @Title: System.java 
* @Package org.porridge.module.util.po 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月21日 下午9:04:58 
* @version V1.0
 */
public class Setting {
	//系统参数
	@Vali({@Verify(name = "empty", reason = "系统名称不能为空"),
		   @Verify(name = "lenmax", reason = "系统名称长度不能超过100",len=100)})
	private String sys_user;//系统名称
	@Vali({@Verify(name = "empty", reason = "seo关键字不能为空"),
		   @Verify(name = "lenmax", reason = "seo长度不能超过100",len=100)})
	private String sys_seo;//主seo关键字
	@Vali({@Verify(name = "empty", reason = "邮件错误提醒地址不能为空"),
		@Verify(name = "email", reason = "邮件错误提醒地址格式错误"),
		   @Verify(name = "lenmax", reason = "邮件错误提醒地址长度不能超过100",len=100)})
	private String error_email;//邮件错误提醒地址
	//系统邮箱设置
	@Vali({@Verify(name = "empty", reason = "邮箱不能为空"),
		@Verify(name = "email", reason = "邮箱地址格式错误"),
		   @Verify(name = "lenmax", reason = "邮箱长度不能超过100",len=100)})
	private String email_user;//邮箱登录名
	@Vali({@Verify(name = "empty", reason = "邮箱密码不能为空"),
		   @Verify(name = "lenmax", reason = "邮件密码长度不能超过100",len=100)})
	private String email_pas;//邮箱密码
	@Vali({@Verify(name = "empty", reason = "邮箱POP不能为空"),
		   @Verify(name = "lenmax", reason = "邮箱POP长度不能超过100",len=100)})
	private String email_pop;//邮箱pop接收地址
	@Vali({@Verify(name = "empty", reason = "邮箱SMTP地址不能为空"),
		   @Verify(name = "lenmax", reason = "邮箱SMTP长度不能超过100",len=100)})
	private String email_smtp;//邮箱smtp发送地址
	private String email_isopen;//开启邮件发文  true则是开启
	//前台页面设置
	@Vali({@Verify(name = "empty", reason = "分页数量不能为空"),
		   @Verify(name="number",reason="分布格式必须是数字"),
		   @Verify(name = "lenmax", reason = "分页数量长度不能超过100",len=100)})
	private String page_size;//每页分页数据
	@Vali({@Verify(name = "empty", reason = "缩略显示长度"),
		   @Verify(name="number",reason="缩略显示长度必须是数字")})
	private String page_breviary;//缩略显示长度
	//七牛帐号设置
	@Vali({@Verify(name = "empty", reason = "七牛登录名不能为空"),
		   @Verify(name = "lenmax", reason = "七牛登录名长度不能超过100",len=100)})
	private String qiniu_account;//登录名
	@Vali({@Verify(name = "empty", reason = "七牛密码不能为空"),
		   @Verify(name = "lenmax", reason = "七牛密码长度不能超过100",len=100)})
	private String qiniu_password;//登录密码
	@Vali({@Verify(name = "empty", reason = "七牛空间名称不能为空"),
		   @Verify(name = "lenmax", reason = "七牛空间名称长度不能超过100",len=100)})
	private String qiniu_name;//空间名称
	@Vali({@Verify(name = "empty", reason = "上传后缀不能为空."),
		   @Verify(name = "lenmax", reason = "上传后缀长度不能超过100",len=100)})
	private String qiniu_suffix;//限制上传后缀
	@Vali({@Verify(name = "lenmax", reason = "上传后缀长度不能超过100",len=100)})
	private String qunui_first;//上传的空间地址前缀
	
	public String getEmail_isopen() {
		return email_isopen;
	}
	public void setEmail_isopen(String email_isopen) {
		this.email_isopen = email_isopen;
	}
	public String getPage_breviary() {
		return page_breviary;
	}
	public void setPage_breviary(String page_breviary) {
		this.page_breviary = page_breviary;
	}
	public String getQunui_first() {
		return qunui_first;
	}
	public void setQunui_first(String qunui_first) {
		this.qunui_first = qunui_first;
	}
	public String getQiniu_suffix() {
		return qiniu_suffix;
	}
	public void setQiniu_suffix(String qiniu_suffix) {
		this.qiniu_suffix = qiniu_suffix;
	}
	public String getSys_user() {
		return sys_user;
	}
	public void setSys_user(String sys_user) {
		this.sys_user = sys_user;
	}
	public String getSys_seo() {
		return sys_seo;
	}
	public void setSys_seo(String sys_seo) {
		this.sys_seo = sys_seo;
	}
	public String getError_email() {
		return error_email;
	}
	public void setError_email(String error_email) {
		this.error_email = error_email;
	}
	public String getEmail_user() {
		return email_user;
	}
	public void setEmail_user(String email_user) {
		this.email_user = email_user;
	}
	public String getEmail_pas() {
		return email_pas;
	}
	public void setEmail_pas(String email_pas) {
		this.email_pas = email_pas;
	}
	public String getEmail_pop() {
		return email_pop;
	}
	public void setEmail_pop(String email_pop) {
		this.email_pop = email_pop;
	}
	public String getEmail_smtp() {
		return email_smtp;
	}
	public void setEmail_smtp(String email_smtp) {
		this.email_smtp = email_smtp;
	}
	public String getPage_size() {
		return page_size;
	}
	public void setPage_size(String page_size) {
		this.page_size = page_size;
	}
	public String getQiniu_account() {
		return qiniu_account;
	}
	public void setQiniu_account(String qiniu_account) {
		this.qiniu_account = qiniu_account;
	}
	public String getQiniu_password() {
		return qiniu_password;
	}
	public void setQiniu_password(String qiniu_password) {
		this.qiniu_password = qiniu_password;
	}
	public String getQiniu_name() {
		return qiniu_name;
	}
	public void setQiniu_name(String qiniu_name) {
		this.qiniu_name = qiniu_name;
	}
}
