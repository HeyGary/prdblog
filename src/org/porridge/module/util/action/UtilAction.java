package org.porridge.module.util.action;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import org.db.createsql.RealizeDaoInter;
import org.porridge.module.fun.po.Fun;
import org.porridge.module.main.po.User;
import org.porridge.module.util.po.Setting;
import org.porridge.util.BaseUtil;
import org.porridge.util.Constant;
import org.porridge.util.EmailUitl;
import org.porridge.util.qiniu.QiniuUtil;
import org.transmit.ActionContext;
import org.transmit.ActionUtil;
import org.transmit.ThreadDao;
import org.transmit.form.vali.ValidationPage;
import org.transmit.key.Action;
import org.transmit.key.ReturnJSON;
import org.transmit.key.ReturnText;
import org.transmit.util.FileUtil;

/**
 * 读配置Action
 * 比如读七牛配置
 * 写系统变量什么的
* @Title: UtilAction.java 
* @Package org.porridge.module.util.action 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月21日 下午9:00:52 
* @version V1.0
 */
@Action("module")
public class UtilAction {
	/**
	 * 读七牛的untoken
	 * @return
	 */
	@ReturnText
	public String getUptoken(){
		return "{\"uptoken\":\""+QiniuUtil.getUptoken(Constant.SETTING.getQiniu_name())+"\",\"qiniu_suffix\":\""+Constant.SETTING.getQiniu_suffix()+"\",\"first\":\""+Constant.SETTING.getQunui_first()+"\"}";
	}
	/**
	 * 保存系统变量
	 */
	 public void saveSetting(){
		 Setting sys=ActionUtil.find(Setting.class);
		 PrintWriter out=ActionContext.getPrintWriter();
		 if(ValidationPage.ValidationPO(sys)){
			 boolean bl=BaseUtil.saveSetting(sys);
			 if(bl){
				 Constant.SETTING=sys;
				 out.print("{\"result\":true}");
			 }else
				 out.print("{\"result\":false,\"name\":null,\"text\":\"保存配置失败\"}");
		 }
	 }
	 /**
	  * 返回配置
	  * @return
	  */
	 @ReturnJSON
	 public Setting findSetting(){
		 return Constant.SETTING;
	 }
}
