package org.porridge.module.fun.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.db.createsql.AbstrDao;
import org.porridge.module.fun.dao.FunDao;
import org.porridge.module.fun.po.Fun;
import org.porridge.module.main.dao.UserDao;
import org.porridge.module.main.po.User;
import org.porridge.util.BaseUtil;
import org.porridge.util.jqgrid.GridPO;

/**
 * 功能表添加
* @Title: FunBusiness.java 
* @Package org.porridge.module.fun.business 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月19日 下午2:44:21 
* @version V1.0
 */
public class FunBusiness  extends AbstrDao<FunDao>{
	
	/**
	 * 根据用户查询他所拥有的权限
	 * 暂时不实现
	 * @param user
	 * @return
	 * @throws Exception 
	 */
	public Map<String,List<Fun>> findUserFun(User user) throws Exception{
		List<Fun> funs=this.absDao.getList(Fun.class,"*","1=1","sort","asc",0,1000);
		int max=0;
		List<Fun> tfs=this.absDao.getList(Fun.class,"funcode","1=1","convert(funcode,signed)","desc",0,1);
		if(tfs.size()>0)
			max=tfs.get(0).getFuncode().length()/4;
		
		Map<String,List<Fun>> funmap=new HashMap<String, List<Fun>>();
		List<Fun> fs=new ArrayList<Fun>();
		for(Fun f:funs){
			if(f.getFuncode().length()==4)
				fs.add(f);
		}
		funmap.put("index",fs);
		for(int i=1;i<max;i++){
			for(Fun fun:funs){
				if(fun.getFuncode().length()==i*4){
					fun.getFuncode();
					fs=new ArrayList<Fun>();
					for(Fun f:funs){
						if(f.getFuncode().length()==i*4+4 && f.getFuncode().substring(0,i*4).equals(fun.getFuncode())){
							fs.add(f);
						}
					}
					funmap.put(fun.getFuncode(),fs);
				}
			}
		}
		return funmap;
	}
	/**
	 * 保存功能表
	 * @param fun
	 * @return
	 * @throws Exception
	 */
	public String saveFun(Fun fun) throws Exception{
		String code="";
		if(fun.getParentid()!=null && fun.getParentid().trim().length()>0){
			Fun tfun=this.absDao.find(Fun.class,fun.getParentid());
			if(tfun!=null)
				code=tfun.getFuncode();
		}
		int max=Integer.valueOf(this.absDao.findFuncode("funcode","parentid='"+fun.getParentid()+"'"));
		code+=BaseUtil.formatCode(max+"",4);
		fun.setFuncode(code);
		fun.setSort(code);
		return this.absDao.save(fun);
	}
	/**
	 * 删除功能表
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public String delFun(String id) throws Exception{
		String ret="{\"result\":false,\"text\":\"功能节点下有子级,不能删除\"}";
		if(this.absDao.getCount(Fun.class,"parentid='"+id+"'")==0){
			int del=this.absDao.deletable(Fun.class,"id='"+id+"'");
			if(del>0)
				ret="{\"result\":true}";
			else
				ret="{\"result\":false,\"text\":\"功能节点删除失败,可能不存在此功能节点\"}";
		}
		return ret;
	}
	/**
	 * 查询功能表jqgrid
	 * @param grph
	 * @return
	 * @throws Exception
	 */
	public String jqgrid(GridPO grph) throws Exception{
		String ret="";
		if(grph!=null){
			ret=this.absDao.getListJqgrid(Fun.class,grph.getFilename(),grph.getWhere(),grph.getSidx(),grph.getSord(),grph.getPage(),grph.getRows());
		}
		return ret;
	}
	/**
	 * 查询树  
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public String findTree(String id) throws Exception{
		List<Fun> funs=this.absDao.getList(Fun.class,"id,name,type","parentid='"+id+"'","sort","asc");
		StringBuffer stb=new StringBuffer();
		for(Fun fun:funs){
			stb.append("{id:'"+fun.getId()+"',name:'"+fun.getName()+"',isParent:"+("2".equals(fun.getType())?"false":"true")+"},");
		}
		if(funs.size()>0)
			return "["+stb.substring(0,stb.length()-1)+"]";
		else
			return "[]";
	}
	/**
	 * 保存排序
	 * @param parid
	 * @return
	 * @throws Exception 
	 */
	public boolean saveFunsort(String ids) throws Exception{
		boolean bl=false;
		if(ids!=null && ids.trim().length()>0){
			String[] thids=ids.split(",");
			Fun fun=null;
			for(int i=0;i<thids.length;i++){
				if(thids[i].trim().length()>0){
					fun=new Fun();
					fun.setSort(BaseUtil.formatCode((i+1)+"",4));
					fun.setId(thids[i].trim());
					this.absDao.update(fun);
				}
			}
			bl=true;
		}
		return bl;
	} 
}
