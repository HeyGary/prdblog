package org.porridge.module.fun.action;

import java.io.PrintWriter;

import org.porridge.module.fun.business.FunBusiness;
import org.porridge.module.fun.po.Fun;
import org.porridge.util.jqgrid.GridPO;
import org.porridge.util.jqgrid.JqgridUtil;
import org.transmit.ActionContext;
import org.transmit.ActionUtil;
import org.transmit.form.vali.ValidationPage;
import org.transmit.key.Action;
import org.transmit.key.Business;
import org.transmit.key.ReturnJSON;
import org.transmit.key.ReturnText;

/**
 * 功能模块添加类
* @Title: FunAction.java 
* @Package org.porridge.module.fun.action 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月19日 下午2:57:28 
* @version V1.0
 */
@Action("module")
public class FunAction {
	@Business
	private FunBusiness funbusiness;
	
	/**
	 * 保存功能
	 * @return
	 * @throws Exception 
	 */
	public void saveFun() throws Exception{
		Fun fun=ActionUtil.find(Fun.class);
		if(ValidationPage.ValidationPO(fun)){
			if(fun.getParentid()!=null)
				fun.setParentid(fun.getParentid().trim());
			else
				fun.setParentid("");
			 PrintWriter out=ActionContext.getPrintWriter();
			 String ret="{\"result\":false,\"name\":null,\"text\":\"保存功能失败\"}";
				 fun.setId(null);
				 if(!"-1".equals(funbusiness.saveFun(fun))){
					 ret="{\"result\":true}";
				 }
				 out.print(ret);;
		}
	}
	/**
	 * 查看一条功能表数据
	 * @return
	 */
	@ReturnJSON
	public Fun findFun() throws Exception{
		Object obj=ActionContext.getRequest().getParameter("id");
		Fun fun=null;
		if(obj!=null && obj.toString().trim().length()>0){
				fun=funbusiness.findPo(Fun.class,obj.toString().trim());
		}
		return fun;
	}
	/**
	 * 修改功能
	 */
	public void updateFun() throws Exception{
		Fun fun=ActionUtil.find(Fun.class);
		if(ValidationPage.ValidationPO(fun)){
			 PrintWriter out=ActionContext.getPrintWriter();
			 String ret="{\"result\":false,\"name\":null,\"text\":\"修改功能失败\"}";
			 	 fun.setFuncode(null);
			 	 fun.setParentid(null);
			 	 fun.setSort(null);
				 if(funbusiness.updatePo(fun)>0){
					 ret="{\"result\":true}";
				 }
				out.print(ret);;
		}
	}
	/**
	 * 删除功能表
	 * @return
	 */
	@ReturnText
	public String delFun() throws Exception{
		Object obj=ActionContext.getRequest().getParameter("id");
		String ret="{\"result\":false,\"text\":\"功能节点删除失败\"}";
		if(obj!=null && obj.toString().trim().length()>0){
				ret=funbusiness.delFun(obj.toString().trim());
		}
		return ret;
	}
	/**
	 * 查询jqgrid表格数据
	 * @return
	 */
	@ReturnText
	public String funjqgrid() throws Exception{
		GridPO grid;
		String ret="";
			grid = JqgridUtil.getGridPath();
			if(grid!=null)
				ret=this.funbusiness.jqgrid(grid);
			return ret;
	}
	/**
	 * 查询树
	 * @return
	 * @throws Exception
	 */
	@ReturnText
	public String findtree() throws Exception{
		String json="[]";
		String id="";
		Object obj=ActionContext.getRequest().getParameter("id");
		if(obj!=null && obj.toString().trim().length()>0)
			id=obj.toString().trim();
		
		json=this.funbusiness.findTree(id);
		return json;
	}
	/**
	 * 保存排序
	 * @return
	 * @throws Exception 
	 */
	@ReturnText
	public String saveFunsort() throws Exception{
		Object obj=ActionContext.getRequest().getParameter("ids");
		boolean bl=false;
		if(obj!=null && obj.toString().trim().length()>0){
			bl=this.funbusiness.saveFunsort(obj.toString());
		}
		return String.valueOf(bl);
	}
}
