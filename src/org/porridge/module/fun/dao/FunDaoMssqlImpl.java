package org.porridge.module.fun.dao;


import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.db.comments.NumException;
import org.db.createsql.AbstrDaoMssql;
import org.porridge.module.fun.po.Fun;
import org.porridge.module.main.po.User;

public class FunDaoMssqlImpl extends AbstrDaoMssql implements FunDao {

	/**
	 * 查询功能表的code最后四位
	 * @throws Exception 
	 */
	@Override
	public String findFuncode(String file,String where) throws Exception{
		String ret=this.tableMax(Fun.class,"substring("+file+",len(convert("+file+",long))-3,len("+file+"))",where);
		return ret==null?"0":ret;
	}
}
