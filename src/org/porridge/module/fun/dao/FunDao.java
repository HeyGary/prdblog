package org.porridge.module.fun.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.db.comments.NumException;
import org.db.createsql.RealizeDaoInter;
import org.porridge.module.main.po.User;

public interface FunDao extends RealizeDaoInter{
	/**
	 * 查询功能表长度
	 * 因为mysql 和 mssql的获取长度不一样.所以需要分开写代码.
	 * @return
	 */
	public String findFuncode(String file,String where) throws Exception;
}
