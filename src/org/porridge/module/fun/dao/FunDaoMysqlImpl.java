package org.porridge.module.fun.dao;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.db.comments.NumException;
import org.db.createsql.AbstrDao;
import org.db.createsql.AbstrDaoMysql;
import org.porridge.module.fun.po.Fun;
import org.porridge.module.main.po.User;

public class FunDaoMysqlImpl extends AbstrDaoMysql implements FunDao {

	@Override
	public String findFuncode(String file, String where) throws Exception {
		String ret=this.tableMax(Fun.class,"substring("+file+",length(CONVERT("+file+",SIGNED))-3,length("+file+"))",where);
		return ret==null?"0":ret;
	}

}
