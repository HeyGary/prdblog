package org.porridge.module.fun.po;

import org.db.comments.Key;
import org.db.comments.Table;
import org.transmit.form.key.Vali;
import org.transmit.form.key.Verify;

/**
 * 功能表模块
* @Title: Fun.java 
* @Package org.porridge.module.fun.po 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月19日 下午2:39:26 
* @version V1.0
 */
@Table(name="sys_fun")
public class Fun {
	@Key
	private String id;
	@Vali({@Verify(name = "empty", reason = "功能名称不能为空"),
		   @Verify(name = "lenmax", reason = "功能名称长度不能超过100",len=100)})
	private String name;
	private String parentid;
	@Vali({@Verify(name = "empty", reason = "类型不能为空"),
		   @Verify(name = "lenmax", reason = "类型长度不能超过100",len=100)})
	private String type;//0无  1菜单   2按钮  3模版
	@Vali({@Verify(name = "lenmax", reason = "ID长度不能超过100",len=100)})
	private String noteid;
	@Vali({@Verify(name = "lenmax", reason = "样式长度不能超过100",len=100)})
	private String cls;
	@Vali({@Verify(name = "lenmax", reason = "连接地址不能超过100",len=100)})
	private String link;
	/**
	<option value="0">菜单</option>
	<option value="1">单页</option>
	<option value="2">列表</option>
	<option value="3">主页</option>
	<option value="4">直连</option>
	 */
	private String ftltype;//静态模版类型
	private String funcode;//功能表代码
	private String ftlpath;//模版地址
	private String sort;//排序
	
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getFtlpath() {
		return ftlpath;
	}
	public void setFtlpath(String ftlpath) {
		this.ftlpath = ftlpath;
	}
	public String getFtltype() {
		return ftltype;
	}
	public void setFtltype(String ftltype) {
		this.ftltype = ftltype;
	}
	public String getFuncode() {
		return funcode;
	}
	public void setFuncode(String funcode) {
		this.funcode = funcode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNoteid() {
		return noteid;
	}
	public void setNoteid(String noteid) {
		this.noteid = noteid;
	}
	public String getCls() {
		return cls;
	}
	public void setCls(String cls) {
		this.cls = cls;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
}
