package org.porridge.module.main.po;

import org.db.comments.Key;
import org.db.comments.Table;
import org.transmit.form.key.Vali;
import org.transmit.form.key.Verify;

/**
 * 用户表
* @Title: User.java 
* @Package org.porridge.module.main.po 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月15日 下午10:22:45 
* @version V1.0
 */
@Table(name="sys_user")
public class User {
	@Key
	private String id;
	@Vali({@Verify(name = "empty", reason = "登录名不能为空"),
		   @Verify(name = "lenmax", reason = "登录名长度不能超过100",len=100)})
	private String username;//登录名
	@Vali({@Verify(name = "empty", reason = "真实姓名不能为空"),
		   @Verify(name = "lenmax", reason = "亲,您不是外国人吧?姓名长度不能超过100",len=100)})
	private String realname;//真实姓名
	@Vali({@Verify(name = "empty", reason = "密码不能为空"),
		   @Verify(name = "lenmin", reason = "密码必须是六位以上",len=6)})
	private String pasword;//密码 md5加密
	private String unitid;//单位id
	@Vali({@Verify(name = "lenmax", reason = "地址长度不能超过500",len=500)})
	private String address;//地址
	@Vali({@Verify(name = "phone", reason = "电话号码格式错误")})
	private String phone;//手机
	@Vali({@Verify(name = "empty", reason = "邮箱地址不能为空"),
		   @Verify(name = "email", reason = "邮箱地址格式错误"),
		   @Verify(name = "lenmax", reason = "邮箱长度不能超过100",len=100)})
	private String mail;//邮箱
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getPasword() {
		return pasword;
	}
	public void setPasword(String pasword) {
		this.pasword = pasword;
	}
	public String getUnitid() {
		return unitid;
	}
	public void setUnitid(String unitid) {
		this.unitid = unitid;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
}
