package org.porridge.module.main.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.db.comments.NumException;
import org.db.createsql.RealizeDaoInter;
import org.porridge.module.main.po.User;

public interface UserDao extends RealizeDaoInter{
	public User userLogin(String username,String pasword) throws Exception;
}
