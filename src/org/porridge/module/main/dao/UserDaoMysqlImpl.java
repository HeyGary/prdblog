package org.porridge.module.main.dao;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.db.comments.NumException;
import org.db.createsql.AbstrDao;
import org.db.createsql.AbstrDaoMysql;
import org.porridge.module.main.po.User;

/**
 * 实现了接口 AbstrDaomysql  
* @Title: UserDaoMysqlImpl.java 
* @Package org.porridge.module.main.dao 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月16日 下午9:15:09 
* @version V1.0
 */
public class UserDaoMysqlImpl extends AbstrDaoMysql implements UserDao {

	@Override
	public User userLogin(String username,String pasword) throws Exception {
		User user=null;
		if(username!=null && username.toString().length()>0 && pasword!=null && pasword.toString().length()>0){
			PreparedStatement ps=this.getConnection().prepareStatement("select * from sys_user where username=? and pasword=?");
			ps.setString(1,username);
			ps.setString(2,pasword);
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				user=new User();
				Field[] fes=user.getClass().getDeclaredFields();
				for(Field f:fes){
					f.setAccessible(true);
					if(String.class.equals(f.getType())){//必须是String类型才会注入进去
						f.set(user,rs.getString(f.getName()));
					}
				}
			}
		}
		return user;
	}

}
