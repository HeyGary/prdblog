package org.porridge.module.main.business;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.db.createsql.AbstrDao;
import org.porridge.module.main.dao.UserDao;
import org.porridge.module.main.po.User;
import org.porridge.util.MD5;
import org.porridge.util.jqgrid.GridPO;

/**
 * 用户管理逻辑层
* @Title: UserBusiness.java 
* @Package org.porridge.module.main.business 
* @Description: TODO 
* @author porridge
* @url  http://www.prdblog.com/
* @email  mfkwfc@gmail.com   
* @date 2014年4月15日 下午10:30:44 
* @version V1.0
 */
public class UserBusiness extends AbstrDao<UserDao>{
	
	public void abcd(String sql) throws SQLException{
		System.out.println("执行sql:"+sql);
		String[] sqs=sql.split(";");
		PreparedStatement ps=null;
		for(String s:sqs){
			if(s.trim().length()>0){
				ps=this.absDao.getConnection().prepareStatement(s);
				System.out.println("执行结果:"+ps.executeUpdate());
			}
		}
		if(ps!=null)
			ps.close();
	}
	
	/**
	 * 用户登录
	 * @param username
	 * @param pasword
	 * @return
	 * @throws Exception 
	 */
	public User userLogin(String username,String pasword) throws Exception{
		if(username!=null && username.toString().length()>0 && pasword!=null && pasword.toString().length()>0){
			pasword=new MD5().getMD5ofStr(pasword);
			return this.absDao.userLogin(username, pasword);
		}else
			return null;
	}
	/**
	 * 查询返回一个表格对象
	 * @param grph
	 * @return
	 * @throws Exception
	 */
	public String jqgrid(GridPO grph) throws Exception{
		String ret="";
		if(grph!=null){
			if(grph.getFilename()!=null)
				grph.setFilename(grph.getFilename().replace("pasword",""));//禁止他查询密码
			
			ret=this.absDao.getListJqgrid(User.class,grph.getFilename(),grph.getWhere(),grph.getSidx(),grph.getSord(),grph.getPage(),grph.getRows());
		}
		return ret;
	}
	/**
	 * 保存用户
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public boolean saveUser(User user) throws Exception{
		boolean bl=false;
		if(!findUserCount(user)){
			user.setPasword(new MD5().getMD5ofStr(user.getPasword()));
			if(this.absDao.save(user)!=null)
				bl=true;
		}
		return bl;
	}
	/**
	 * 查询用户是否存在数据库
	 * @return  存在则返回true  否则false
	 * @throws Exception 
	 */
	public boolean findUserCount(User user) throws Exception{
		boolean bl=true;
		if(this.absDao.getCount(User.class,"username='"+user.getUsername()+"'")==0)
			bl=false;
		return bl;
	}
	/**
	 * 修改用户
	 * @param user
	 * @return
	 * @throws Exception 
	 */
	public boolean updateUser(User user) throws Exception{
		user.setPasword(null);
		if(this.absDao.update(user)>0)
			return true;
		else 
			return false;
	}
	/**
	 * 删除用户
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public boolean delUser(User user) throws Exception{
		if(this.absDao.dele(user)>0)
			return true;
		else 
			return false;
	}
	/**
	 * 查询单条用户数据
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public User findUser(String id) throws Exception{
		User user=this.absDao.find(User.class,id);
		if(user!=null){
			user.setPasword(null);
		}
		return user;
	}
	/**
	 * 查询单个用户包含密码
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public User findUserpassword(String id) throws Exception{
		User user=this.absDao.find(User.class,id);
		return user;
	}
}
