/**
 *  工具类js 
 */
(function($){
	$.parseObj=function(strData){//转换对象
	    return (new Function( "return " + strData ))();
	};
	$.getseachwhere=function(id){
			var ret="1=1";
			$("#"+id+" input[name][type=text]").each(function(){
				    var tv=$(this).val();
					if(""!=tv && tv!=null){
						if($.trim(tv)!=""){
							var tname=$(this).attr("name");
							var t_wh=(tname.indexOf(",")==-1?false:true);
							var twhere="";
							while(true){
								index=tname.indexOf(",");
								if(index!=-1){
									var t=tname.substring(0,index);
									tname=tname.substring(index+1,tname.length);
									twhere+=" "+(t_wh?"or":"and")+" "+t+" like '%"+tv+"%' ";
								}else{
									twhere+=" "+(t_wh?"or":"and")+" "+tname+" like '%"+tv+"%' ";
									break;
								}
							}
							if(t_wh){
								ret+=" and (1=2 "+twhere+")";
							}else
								ret+=twhere;
						}
					}
			});
			var tw="";
			$("#"+id+" input[name][type=checkbox]:checked").each(function(){
					var tv=$(this).val();
					if(""!=tv && tv!=null){
						if($.trim(tv)!="")
							tw+=" or "+$(this).attr("name")+"='"+tv+"' ";
					}
			});
			if(tw!="")
				ret+=" and (1=2 "+tw+")";
			
			$("#"+id+" select[name]").each(function(){
				var tv=$(this).val();
				if("null"!=tv && tv!=null && tv!=""){
					if($.trim(tv)!="")
						ret+=" and "+$(this).attr("name")+" like '%"+tv+"%' ";
				}
			});
			return ret;
		};
		$.subhideform=function(href,sub,json){//生成隐藏表单提交数据  参数一提交地址  参数二 是否打开新页面提交   参数三提交参数
			if($("#subhideform").length==0)
				$("body").append("<form style=\"display: none;\" method=\"post\" id=\"subhideform\"></form>");
			$("#subhideform").attr("action",href);
			if(sub){
				$("#subhideform").attr("target","_blank");
			}else{
				$("#subhideform").removeAttr("target");
			}
			var ht="";
			if(json!=null){
				$.each(json,function(k,v){
					ht+="<textarea name=\""+k+"\">"+v+"</textarea>";
				});
			}
			$("#subhideform").html(ht);
			$("#subhideform").submit();
		};
})(jQuery);

/**
 * 表格生成类
 */
$(function(){
	$(".auto_startgrid[id]").each(function(){
			var json=jqgridconfig(this.id);
			$("#"+this.id).jqGrid({ 
				url:json.url, 
				datatype: "json",
				colNames:json.colNames,
				//字段属性详解  hidden:true 是否隐藏 
				colModel:json.colModel, 
	           rowNum:20,//初始请求行数
	           height:"470px",//高度
	           rowList:[20,50,100],//分页行数
	           mtype:"post",//请求方式
	           pager: '#'+json.pageid,//分页的地址
	           sortname:json.sortname,//默认排序id
	           viewrecords: true,//是否要显示总记录数
	           sortorder:json.sortorder,//排序标准
	           caption:"",//表格标题
	           postData:json.postData,//附加参数
	           rownumbers:true,//是否显示序号
	           autowidth:true
	           }).navGrid('#'+json.pageid,{edit:false,add:false,del:false,view:true});
			if($("#"+this.id+"search").length==1){
				var tid=this.id;
				$("#"+this.id+"search").click(function(){
					$("#"+tid).jqGrid('setGridParam',{postData:{"where":$.getseachwhere($("#"+tid+"search").parents("div[id]").attr("id"))}}).trigger("reloadGrid");
				});
			}
	});
});
function jqgridconfig(id){
	if($("#"+id).length==1){
		var pageid=id+"_page"+Math.floor(Math.random()*(123456+1));
		$('#'+id).after('<div id="'+pageid+'"></div>');
		
		var json={url:$("#"+id).attr("action"),
				  pageid:pageid,
				  sortorder:$("#"+id).attr("sortorder")};
		
		var sortname=$("#"+id).attr("sortname");//排序
		if(!(sortname!=null && sortname!=""))
			sortname="id";
		json.sortname=sortname;
		
		var sortorder=$("#"+id).attr("sortorder");
		if(!(sortorder=="asc" || sortorder=="desc"))
			sortorder="asc";
		json.sortorder=sortorder;
		
		
		json.colNames=[];
		json.colModel=[];
		var names="id,";
		$("#"+id+" th").each(function(i){
			json.colNames[i]=$.trim($(this).text());
			var name=$(this).attr("name");
			var col={name:name,index:name,width:$(this).attr("width"),search:true,hidden:($(this).attr("hidden")!=null?true:false)};
			var formatter=$(this).attr("formatter");
			if(formatter!=null && formatter!=""){
				try{
					col.formatter=$.parseObj(formatter);
				}catch(e){
					alert(name+"行回调失败"+e);
				}
			}
			json.colModel[i]=col;
			names+=name+",";
		});
		if($("#"+id+" th").length>0){
			var fixedwhere=$("#"+id).attr("fixedwhere");//固定条件
			if(!(fixedwhere!=null && $.trim(fixedwhere)!=""))
				fixedwhere="1=1";
			json.postData={"filename":names.substring(0,names.length-1),fixedwhere:fixedwhere};
			
		}
		$("#"+id).html("").show(0);
		return json;
	}
}
/**
 * 重新生成静态页面
 */
function refreshftl(){
	art.dialog({id:"refreshftl",content: '生成页面中,请等待...',lock:true});
	$.post(getProjectName()+"/prd/article_RefreshCreateHtml.action",function(data){
			if(data=="true"){
				art.dialog({id:"refreshftl"}).content("静态页面生成成功...").time(1500);
			}else{
				art.dialog({id:"refreshftl"}).content("<span class='red'>静态页面生成失败...</span>").time(1500);
			}
	},"html");
}
/**
*处理导航类
*/
$(function(){
	var url=window.location.pathname;//当前访问地址
	$("#sidebar ul a[href]").each(function(){
		if($(this).attr("href")==url){
			var ht="<li class=\"active\">"+$("#sidebar > a[href='#"+$(this).parents("ul[id]").attr("id")+"']").text()+"<span class=\"divider\">/</span></li>";
			$(this).parents("ul[id]").addClass("in");
			ht+="<li class=\"active\">"+$(this).text()+"</li>";
			$("#navigation").append(ht);
			return false;//中止遍历
		}
	});
});