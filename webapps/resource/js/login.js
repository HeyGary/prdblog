$("#loginbut").removeAttr("disabled");
function getimg(){
	$("#myimg").attr("src",$("#myimg").attr("backsrc")+"?temp"+Math.random());
}
function login(){
	if(valiLogin()){
		$("#loginbut").val("Loading...").attr("disabled","disabled");
		$.post($("#myform").attr("action"),$("#myform").serialize(),function(data){
			if(data==0){
				$("#loginbut").val("login success");
				setTimeout(function(){
					window.location.href=$("#myform").attr("success");
				},2000);
			}else{
				$("#loginbut").val("Sign In").removeAttr("disabled");
				//0成功登录  1用户名或密码为空  2用户或密码错误  3验证码错误  4数据库连接错误
				var tip="Unknown error";
				switch(parseInt(data)){
					case 1:tip="username or password is null,please try again";break;
					case 2:tip="username or password read error";break;
					case 3:tip="Verification code error";break;
					case 4:tip="database link error,Please contact admin";break;
				}
				
				$("#open_message_content").html('<span class="red">'+tip+'</span>');
				$('#open_message').modal('show');
				$("#valiimgdiv").show(0);
				getimg();
			}
		},"html");
	}
}
function valiLogin(){
	var tip="";
	$("#myform input:visible").each(function(){
			if($(this).val()==""){
				tip+=$(this).attr("tip")+"  not't is null<br/>";
			}
	});
	if(tip=="")
		return true;
	else{
		$("#open_message_content").html('<span class="red">'+tip+'</span>');
		$('#open_message').modal('show');
		return false;
	}
}
document.onkeydown = function (e) { 
	var theEvent = window.event || e; 
	var code = theEvent.keyCode || theEvent.which; 
	if (code == 13) { 
		login();
	} 
}