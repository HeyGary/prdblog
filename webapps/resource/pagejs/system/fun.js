/**
 * 功能管理模块
 */
var fun={
		treeNode:null,//ztree选中id
		toggle:function(i){//0 显示数据   1显示添加表单
			if(i==0){
				$("#myTabContent").hide(0);
				$("#showdata").show(0);
			}else{
				$("#myform .help-inline").remove();
				$("#myform .error").removeClass("error");
				$("#myform :input").val("");
				$("#myTabContent").show(0);
				$("#showdata").hide(0);
				if(fun.treeNode!=null)
					$("#fun_parentname").text(fun.treeNode.name);
				else
					$("#fun_parentname").text("根节点");
			}
		},savefun:function(){
			var type=true;//true代表添加   false代表修改
			var hiid=$("#myform input[name=id]").val();
			if(hiid!=null && hiid!="")
				type=false;
			var url=$("#myform").attr("action");
			if(!type)
				url=getProjectName()+"/module/fun_updateFun.action";
			if(fun.treeNode!=null)
				$("#parentid").val(fun.treeNode.id);
			else
				$("#parentid").val("");
			
			$.post(url,$("#myform").serialize(),function(data){
				if(data.result){
					fun.toggle(0);
					$("#fungrid").trigger("reloadGrid");
					fun.refreshtree();
				}else{
					var ther=null;
					if(data.name!=null && data.name!=""){
						ther=$("#myform :input[name='"+data.name+"']").parents(".control-group");
					}else{
						ther=$("#myform .control-group:eq(0)");
					}
					$("#myform .error").removeClass("error");
					$("#myform .help-inline").remove();
					$(ther).addClass("error");
					$(ther).find(":input:eq(0)").focus().after('<span class="help-inline">'+data.text+'</span>');
				}
			},"json");
		},delfun:function(){
			var seid=$("#fungrid").jqGrid('getGridParam','selrow');
			if(seid){
				art.dialog({
					content:"确认删除选中数据?",
					lock:true,
				    ok: function () {
				        $.post(getProjectName()+"/module/fun_delFun.action",{"id":seid},function(data){
				        	if(data.result){
				        		$("#fungrid").jqGrid('delRowData',seid);
				        		fun.refreshtree();
				        	}else{
				        		art.dialog({content: '<span style="color:red;">删除失败,'+data.text+'</span>',lock:true}).time(1500);
				        	}
				        },"json");
				        return true;
				    },cancel:true
				});
			}else{
				art.dialog({content: '<span style="color:red;">请选中一行后再进行操作</span>',lock:true}).time(1500);
			}
		},loadfun:function(){
			var seid=$("#fungrid").jqGrid('getGridParam','selrow');
			if(seid){
				$.post(getProjectName()+"/module/fun_findFun.action",{"id":seid},function(data){
					fun.toggle(1);
					$("#fun_parentname").text("修改节点");
					$.each(data[0],function(k,v){
						$("#myform :input[name='"+k+"']").val(v);
					});
				},"json");
			}else{
				art.dialog({content: '<span style="color:red;">请选中一行后再进行操作</span>',lock:true}).time(1500);
			}
		},formattertype:function(val,obj){//类型回调函数
			var ret="未定义";
			if(val=="0")
				ret="无";
			else if(val=="1")
				ret="菜单";
			else if(val=="2")
				ret="按钮";
			else if(val=="3")
				ret="模版";
			return ret;
		},loadtree:function(){
			var setting = {
					async: {
						enable: true,
						url:getProjectName()+"/module/fun_findtree.action",
						autoParam:["id"]
					},callback: {
						onClick:function(event, treeId, treeNode){
							fun.treeNode=treeNode;
							$("#fungrid").jqGrid('setGridParam',{postData:{"fixedwhere":"parentid='"+treeNode.id+"'"}}).trigger("reloadGrid");
						}
					}
				};
				$.fn.zTree.init($("#ztree"), setting);
		},refreshtree:function(){//刷新节点
			$.fn.zTree.getZTreeObj("ztree").reAsyncChildNodes(fun.treeNode,"refresh");
		},sort:function(){
			var seid="";
			if(fun.treeNode!=null)
				seid=fun.treeNode.id;
			$.post(getProjectName()+"/module/fun_findtree.action",{"id":seid},function(data){
				data=$.parseObj(data);
				var ht="";
				$.each(data,function(k,v){
					ht+="<li id='"+v.id+"'>"+v.name+"</li>";
				});
				$("#sortul").html(ht);
				if(data.length>1){
					art.dialog({
						content:document.getElementById("sortul"),
						lock:true,
					    ok: function () {
					    	var ids="";
					    	$("#sortul li").each(function(){
					    		ids+=this.id+",";
					    	});
					    	$.post(getProjectName()+"/module/fun_saveFunsort.action",{"ids":ids},function(da){
					    		if(da=="true"){
					    			$("#fungrid").trigger("reloadGrid");
									fun.refreshtree();
					    		}else
					    			art.dialog({content: '<span style="color:red;">排序失败,请重试</span>',lock:true}).time(1500);
					    	},"html");
					        return true;
					    },cancel:true
					});
				}else{
					art.dialog({content: '<span style="color:red;">该节点下无二个以上节点.无法进行排序</span>',lock:true}).time(1500);
				}
			},"html");
		}
}