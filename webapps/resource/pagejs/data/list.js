/**
 * 数据管理模块
 */
var list={
		add:function(){
				$.subhideform(getProjectName()+"/pages/background/data/add.jsp",false,{"code":thcode,"type":"add"});
		},update:function(){
			var seid=$("#listgrid").jqGrid('getGridParam','selrow');
			if(seid){
				$.subhideform(getProjectName()+"/pages/background/data/add.jsp",false,{"code":thcode,"pid":seid,"type":"update"});
			}else{
				art.dialog({content: '<span style="color:red;">请选中一行后再进行操作</span>',lock:true}).time(1500);
			}
		},del:function(){
			var seid=$("#listgrid").jqGrid('getGridParam','selrow');
			if(seid){
				art.dialog({
					content:"确认删除选中数据?",
					lock:true,
				    ok: function () {
				        $.post(getProjectName()+"/prd/article_delArticle.action",{"id":seid},function(data){
				        	if(data=="true"){
				        		$("#listgrid").jqGrid('delRowData',seid);
				        		fun.refreshtree();
				        	}else{
				        		art.dialog({content: '<span style="color:red;">删除失败</span>',lock:true}).time(1500);
				        	}
				        },"html");
				        return true;
				    },cancel:true
				});
			}else{
				art.dialog({content: '<span style="color:red;">请选中一行后再进行操作</span>',lock:true}).time(1500);
			}
		},find:function(){
			var seid=$("#listgrid").jqGrid('getGridParam','selrow');
			if(seid){
					$.subhideform(getProjectName()+"/pages/background/data/add.jsp",false,{"code":thcode,"pid":seid,"type":"find"});
			}else{
				art.dialog({content: '<span style="color:red;">请选中一行后再进行操作</span>',lock:true}).time(1500);
			}
		},funtop:function(val,obj){
				return "<a class=\"topa\" href=\"javascript:list.topone('"+obj.rowId+"')\">"+(val=="true"?"取消":"置顶")+"</a>";
		},topone:function(id){
			$.post(getProjectName()+"/prd/article_topArticle.action",{"id":id},function(data){
				if(data=="true"){
					$("#listgridsearch").click();
				}else{
					art.dialog({content: '<span style="color:red;">操作失败</span>',lock:true}).time(1500);
				}
			},"text");
		}
}