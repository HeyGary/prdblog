/**
 * 用户管理模块
 */
var label={
		toggle:function(i){//0 显示数据   1显示添加表单
			if(i==0){
				$("#myTabContent").hide(0);
				$("#showdata").show(0);
			}else{
				$("#myform .help-inline").remove();
				$("#myform .error").removeClass("error");
				$("#myform :input").val("");
				$("#form_title").val("添加标签");
				
				$("#myform div[pas]").show(0);
				$("#myTabContent").show(0);
				$("#showdata").hide(0);
			}
		},savelabel:function(){
			var type=true;//true代表添加   false代表修改
			var hiid=$("#myform input[name=id]").val();
			if(hiid!=null && hiid!="")
				type=false;
			var url=$("#myform").attr("action");
			if(!type)
				url=getProjectName()+"/prd/label_updateLabel.action";
		    
			$.post(url,$("#myform").serialize(),function(data){
				if(data.result){
					label.toggle(0);
					$("#labelgrid").trigger("reloadGrid");
				}else{
					var ther=null;
					if(data.name!=null && data.name!=""){
						ther=$("#myform :input[name='"+data.name+"']").parents(".control-group");
					}else{
						ther=$("#myform .control-group:eq(0)");
					}
					$("#myform .error").removeClass("error");
					$("#myform .help-inline").remove();
					$(ther).addClass("error");
					$(ther).find(":input:eq(0)").focus().after('<span class="help-inline">'+data.text+'</span>');
				}
			},"json");
		},dellabel:function(){
			var seid=$("#labelgrid").jqGrid('getGridParam','selrow');
			if(seid){
				art.dialog({
					content:"确认删除选中数据?",
					lock:true,
				    ok: function () {
				        $.post(getProjectName()+"/prd/label_delLabel.action",{"id":seid},function(data){
				        	if(data=="true"){
				        		$("#labelgrid").jqGrid('delRowData',seid);
				        	}else{
				        		art.dialog({content: '<span style="color:red;">删除失败</span>',lock:true}).time(1500);
				        	}
				        },"html");
				        return true;
				    },cancel:true
				});
			}else{
				art.dialog({content: '<span style="color:red;">请选中一行后再进行操作</span>',lock:true}).time(1500);
			}
		},loadlabel:function(){
			var seid=$("#labelgrid").jqGrid('getGridParam','selrow');
			if(seid){
				$.post(getProjectName()+"/prd/label_findLabel.action",{"id":seid},function(data){
					label.toggle(1);
					$("#form_title").text("修改用户");
					$("#myform div[pas]").hide(0);
					$.each(data[0],function(k,v){
						$("#myform :input[name='"+k+"']").val(v);
					});
				},"json");
			}else{
				art.dialog({content: '<span style="color:red;">请选中一行后再进行操作</span>',lock:true}).time(1500);
			}
		}
}