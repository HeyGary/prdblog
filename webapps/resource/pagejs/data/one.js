/**
 * 文章
 */
var one={
		editor:null,
		init:function(){
			this.clicklabel();
		},clicklabel:function(){//选择标签
			$('#label').AutoComplete({
			    'data':getProjectName()+"/prd/label_findLables.action",
			    'width': 'auto',
			    'ajaxDataType': 'json',
			    'ajaxType':"post",
			    'onerror': function(msg){alert("加载下拉框失败"+msg);},
			    'afterSelectedHandler': function(data){
			    	one.addlabile(data.value);
		        }
			});
			$("#label").keydown(function(event){
				  if(event.keyCode==13 && $('#label').val()!=""){
					  one.addlabile($('#label').val());
				  }
			});
		},addlabile:function(text){//回车选中某个标签时
			if($("#findlabelul li[title='"+text+"']").length==0){
				$("#findlabelul").append("<li title=\""+text+"\">"+text+"<span onclick=\"one.dellabelli(this)\">x</span></li>");
				var vl=$("#lablename").val();
				if(vl==null)vl="";
				vl=vl.replace(text+",","");
				$("#lablename").val(vl+text+",");
			}
			$("#label").val("");
		},dellabelli:function(th){//删除标签
			var vl=$("#lablename").val();
			vl=vl.replace($(th).parent("li").attr("title")+",","");
			$("#lablename").val(vl);
			$(th).parent("li").remove();
		},saveOne:function(){
			var type=true;//true代表添加   false代表修改
			var hiid=$("#myform input[name=id]").val();
			if(hiid!=null && hiid!="")
				type=false;
			var url=$("#myform").attr("action");
			if(!type)
				url=getProjectName()+"/prd/article_updateArticle.action";
			$("#atr_content").val(UE.getEditor(one.editor).getContent());
			$.post(url,$("#myform").serialize(),function(data){
				if(data.result){
					$("#myform .error").removeClass("error");
					$("#myform .help-inline").remove();
					art.dialog({content: '操作成功,马上刷新页面',lock:true}).time(1500);
					window.location.href=$("#sidebar a[code='"+thcode+"']").attr("href");
				}else{
					var ther=null;
					if(data.name!=null && data.name!=""){
						ther=$("#myform :input[name='"+data.name+"']").parents(".control-group");
					}else{
						ther=$("#myform .control-group:eq(0)");
					}
					$("#myform .error").removeClass("error");
					$("#myform .help-inline").remove();
					$(ther).addClass("error");
					$(ther).find(":input:eq(0)").focus().after('<span class="help-inline">'+data.text+'</span>');
				}
			},"json");
		},retpath:function(){
			window.location.href=$("#sidebar a[code='"+thcode+"']").attr("href");
		},loadone:function(id){//加载一篇文章
			$.post(getProjectName()+"/prd/article_findArticle.action",{"id":id},function(data){
				$.each(data[0],function(k,v){
					$("#myform :input[name="+k+"]").val(v);
				});
				UE.getEditor(one.editor).setContent(data[0].atr_content);
				var labs=data[0].lablename.split(",");
				for(var l in labs){
					if(labs[l]!=null && labs[l]!="")
						one.addlabile(labs[l]);
				}
			},"json");
		},loadcode:function(code){
			$.post(getProjectName()+"/prd/article_findArticleCode.action",{"code":code},function(data){
				$.each(data[0],function(k,v){
					$("#myform :input[name="+k+"]").val(v);
				});
				UE.getEditor(one.editor).setContent(data[0].atr_content);
				var labs=data[0].lablename.split(",");
				for(var l in labs){
					if(labs[l]!=null && labs[l]!="")
						one.addlabile(labs[l]);
				}
			},"json");
		}
}