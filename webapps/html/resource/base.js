/**
 * 前台页面支撑js
 * powered by Porridge
 */
//向上精简js
$(function() {
	$.fn.manhuatoTop = function(options) {//向上插件
		var defaults = {			
			showHeight : 150,
			speed : 1000
		};
		var options = $.extend(defaults,options);
		$("body").prepend("<div id='totop'><span>&nbsp;</span></div>");
		var $toTop = $(this);
		var $top = $("#totop");
		var $ta = $("#totop span");
		$toTop.scroll(function(){
			var scrolltop=$(this).scrollTop();		
			if(scrolltop>=options.showHeight){				
				$top.show();
			}
			else{
				$top.hide();
			}
		});	
		$top.click(function(){
			$("html,body").animate({scrollTop:0}, options.speed);	
		});
	}
	$.fn.scrollH=function(){//注册目录事件
		if($("#catalog").length==1){
			$("#catalog").appendTo(".right_menu");
			var right=$(".right_menu").offset().top+$(".right_menu").height();
			if($("#catalog").height()+100>$(window).height()){
				$("#catalog").height($("#catalog").height()-(($("#catalog").height()+100)-$(window).height())-30);
				$("#catalog").css("overflow","scroll");
			}
			$(this).scroll(function(){
				if(($(this).scrollTop()+$("#catalog").height()+30)>right)
					$("#catalog").addClass("catalog");
				else
					$("#catalog").removeClass("catalog");
			});
			$("#catalog dt").bind("click",function(i){//h1点击
				 var eq=$(this).attr("eq");
				 $("html,body").animate({scrollTop:$("#article h1:eq("+eq+")").offset().top},200);
			});
			$("#catalog dd").bind("click",function(i){//h2点击
				 var eq=$(this).attr("eq");
				 $("html,body").animate({scrollTop:$("#article h2:eq("+eq+")").offset().top},200);
			});
		}
	}
});

$(function(){
	$.pjax({
	    selector:"a[href][href!='#'][href!='javascript:void(0);'][href!='javascript:;']:not([target],'.item')",//pjax只触发有href属性的  以及不处理新弹出窗口的,以及不处理样式为item类型的代码高亮A标签
	    container: '#content', //内容替换的容器
	    show: 'fade',  //展现的动画，支持默认和fade, 可以自定义动画方式，这里为自定义的function即可。
	    cache:true,  //是否使用缓存
	    storage:true,  //是否使用本地存储
	    titleSuffix: '', //标题后缀
	    filter: function(){
			    $(".right_menu #catalog").remove();
	    		$("#totop").click();
				$(".main").css("height","auto");
	    		setTimeout(function(){//pjax加载后延迟一下渲染代码.
	    			SyntaxHighlighter.highlight();
	    			$(window).scrollH();
					automainheight();
	    		},1000);
	    },
	    callback: function(){pajx_loadDuodsuo();}
	});
	$(window).manhuatoTop({showHeight:200,speed:100});//注册向上插件
	$(window).scrollH();
	$(window).keydown(function(event){//快捷翻页
		  switch(event.keyCode){
			  	case 37:$("#prev_page").click();break;//上
			  	case 39:$("#next_page").click();break;//下
		  }
	});
	try{
		var cal=new mod_calendar();
		cal.datajson=eval($("#mod_calendar").attr("datajson"));
		if(cal.datajson.length>0){
			cal.gotodate(parseInt(cal.datajson[0].name.substring(0,4)));
			$("#mod_calendar .calendar_next").bind("click",function(){cal.nextdate();});
			$("#mod_calendar .calendar_prev").bind("click",function(){cal.prevdate();});
		}
	}catch(e){return false;}//有可能会有异常.所以检测抛出 
	SyntaxHighlighter.highlight();
	automainheight();
});
/**
 * 处理日志归档功能
 */
function mod_calendar(){
	this.datajson=null,
	this.date=null,
	this.gotodate=function(date){
		this.date=date;
		$("#mod_calendar .year").text(date+"年");
		$("#mod_calendar .calendar_month li").each(function(i){
			$(this).text((i+1)+"月");
		});
		$.each(this.datajson,function(k,v){
			if(date==v.name.substring(0,4)){
				var month=(parseInt(v.name.substring(5,7)));
				$("#mod_calendar .calendar_month li:eq("+(month-1)+")").html("<a title=\"本月共有"+v.number+"篇文章\" href=\""+v.link+"\">"+month+"月</a>");
			}
		});
	},this.nextdate=function(){
		if(this.date!=null && this.date>=2000){
			++this.date;
			this.gotodate(this.date);
		}
	},this.prevdate=function(){
		if(this.date!=null && this.date<=3000){
			--this.date;
			this.gotodate(this.date);
		}
	};
}
function automainheight(){//自动调整黑色底部的高度
	$("#article a:not([target])").attr("target","_blank");
	return;
	if(window.screen.availHeight>$("body").height()){
		$(".main").css("height","auto");
		if($("body").height()<$(document).height())
			$(".main").css("height",$(".main").height()+($(window).height()-$("body").height())+20);
	}
}
/**
 * pjax后需要回调函数.加载多说
 */
function pajx_loadDuodsuo(){
	var dus=$(".ds-thread");
	if($(dus).length==1){
		var el = document.createElement('div');
		el.setAttribute('data-thread-key',$(dus).attr("data-thread-key"));//必选参数
		el.setAttribute('data-url',$(dus).attr("data-url"));
		DUOSHUO.EmbedThread(el);
		$(dus).html(el);
		automainheight();
	}
}
//加载多说
var duoshuoQuery = {short_name:"prdblog"};
(function() {
	var ds = document.createElement('script');
	ds.type = 'text/javascript';ds.async = true;
	ds.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//static.duoshuo.com/embed.js';
	ds.charset = 'UTF-8';
	(document.getElementsByTagName('head')[0] 
	 || document.getElementsByTagName('body')[0]).appendChild(ds);
})();