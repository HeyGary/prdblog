<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
<meta charset="utf-8">
<title>登录页面</title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/blue/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/blue/stylesheets/theme.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/blue/lib/font-awesome/css/font-awesome.css">
<script src="${pageContext.request.contextPath}/resource/blue/lib/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/blue/lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">function getProjectName(){return "${pageContext.request.contextPath}";}</script>
<script src="${pageContext.request.contextPath}/resource/js/login.js" type="text/javascript"></script>
</head>
 <body>
    <div class="navbar">
        <div class="navbar-inner">
                <a class="brand" target="_blank" href="http://www.prdblog.com/"><span class="first">小米粥</span> <span class="second">prdblog</span></a>
        </div>
    </div>
        <div class="row-fluid">
    <div class="dialog">
        <div class="block">
            <p class="block-heading">Sign In</p>
            <div class="block-body">
                <form success="${pageContext.request.contextPath}/pages/background/index.jsp" id="myform" action="${pageContext.request.contextPath}/module/user_userLogin.action" method="post" >
                    <label>Username</label>
                    <input name="username" tip="Username" type="text" class="span12">
                    <label>Password</label>
                    <input type="password" tip="Password" name="pasword" class="span12">
                    <div id="valiimgdiv" style="${sessionScope.valiimg?"":"display:none;"}">
					<div>Verification code</div><input name="imgcode" id="verification" tip="Verification code" class="shortinput" type="text"/>
					<img backsrc="${pageContext.request.contextPath}/module/user_getimgcode.action" src="${pageContext.request.contextPath}${sessionScope.valiimg?"/module/user_getimgcode.action":""}" onclick="getimg()" id="myimg">
					</div>
                    <input type="button" id="loginbut" onclick="login()" class="btn btn-primary pull-right" value="Sign In" />
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- 弹出框 -->
<div id="open_message" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">消息提示</h3>
  </div>
  <div class="modal-body">
    <p id="open_message_content"></p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">知道了</button>
  </div>
</div>
 </body>
</html>