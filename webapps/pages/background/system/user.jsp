<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<title>用户管理</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/pagejs/system/user.js"></script>
</head>
<body>
<div id="showdata">
<div id="seach_div"  class="form-inline btn-toolbar">
    <input class="input-xlarge" name="username,realname" placeholder="Search..." type="text">
    <button class="btn" id="usergridsearch" type="button"><i class="icon-search"></i>Seach</button>
</div>
<jsp:include page="/pages/template/include/funbut.jsp">
<jsp:param value="00010001" name="code"/>
</jsp:include>
<table class="auto_startgrid" id="usergrid" action="${pageContext.request.contextPath}/module/user_userjqgrid.action">
<tr>
	<th name="username" width="100">登录名</th>
	<th name="realname" width="100">用户名</th>
	<th name="phone" width="100">手机</th>
	<th name="address" width="100">地址</th>
</tr>
</table>
</div>
<div id="myTabContent"  style="display:none;" class="tab-content">
<div class="page-header">
  <h2 style="text-indent:230px;" id="form_title">添加用户</h2>
</div>
    <form id="myform" action="${pageContext.request.contextPath}/module/user_saveUser.action" class="form-horizontal">
    	<input type="hidden" name="id"/>
	     <div class="control-group"><label class="control-label" for="username">登录名<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="username" name="username" placeholder="登录系统使用的用户名" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="realname">真实姓名<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="realname" name="realname" placeholder="您的真实姓名,显示在系统内" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div pas="true" class="control-group"><label class="control-label" for="pasword">登录密码<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="password" id="pasword" name="pasword" placeholder="登录密码,建议六位字母,数字,特殊字符组合" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div pas="true" class="control-group"><label class="control-label" for="pasword1">重复密码<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="password" id="pasword1" name="pasword1" placeholder="重复一次登录密码" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="phone">手机</label>
	     		<div class="controls">
	      			<input type="text" id="phone" name="phone" placeholder="您的手机号码" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="mail">邮箱<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="mail" name="mail" placeholder="您的邮箱,可能会接收到系统发送的邮件" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="address">地址</label>
	     		<div class="controls">
	      			<textarea id="address" name="address" placeholder="您所在的地址" class="input-xlarge"></textarea>
	    		</div>
	  	 </div>
      	 <div class="form-actions">
		  		<button type="button" onclick="user.saveuser()" class="btn btn-primary">确定</button>
		  		<button type="button" onclick="user.toggle(0)" class="btn">取消</button>
		</div>
    </form>
      </div>
</body>
</html>