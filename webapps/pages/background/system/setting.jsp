<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>系统设置</title>
<script type="text/javascript">
$(function(){
	$.post(getProjectName()+"/module/util_findSetting.action",function(data){
		$.each(data[0],function(k,v){
			switch($("#myform :input[name='"+k+"']").attr("type")){
				case "text":$("#myform :input[name='"+k+"']").val(v); break;
				case "radio":$("#myform :input[name='"+k+"'][value='"+v+"']").click(); break;
			}
		});
	},"json");
	$("#save").click(function(){
		$.post($("#myform").attr("action"),$("#myform").serialize(),function(data){
			if(data.result){
				art.dialog({content: '保存配置文件成功',lock:true}).time(1500);
				$("#myform .error").removeClass("error");
				$("#myform .help-inline").remove();
			}else{
				var ther=null;
				if(data.name!=null && data.name!=""){
					ther=$("#myform :input[name='"+data.name+"']").parents(".control-group");
				}else{
					ther=$("#myform .control-group:eq(0)");
				}
				$("#myform .error").removeClass("error");
				$("#myform .help-inline").remove();
				$(ther).addClass("error");
				$(ther).find(":input:eq(0)").focus().after('<span class="help-inline">'+data.text+'</span>');
			}
		},"json");
	});
	$("#navigation").append("<li class=\"active\">系统设置</li>");
});
</script>
</head>
<body>
<div class="tab-content">
<div class="page-header">
  <h2 style="text-indent:230px;" id="form_title">系统设置</h2>
</div>
    <form id="myform" action="${pageContext.request.contextPath}/module/util_saveSetting.action" class="form-horizontal">
    	 <blockquote><p>系统参数</p></blockquote>
	  	 <div class="control-group"><label class="control-label" for="sys_user">系统名称<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="sys_user" name="sys_user" placeholder="前台显示系统名称" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="sys_seo">SEO关键字<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="sys_seo" name="sys_seo" placeholder="网站主SEO关键字" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	  <div class="control-group"><label class="control-label" for="error_email">系统错误提醒邮件<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="error_email" name="error_email" placeholder="您接收错误提醒邮件的邮箱地址" class="input-xlarge" />
	    		</div>
	  	 </div>
    	 <blockquote><p>系统发送邮箱设置</p></blockquote>
	     <div class="control-group"><label class="control-label" for="email_user">邮箱登录名<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="email_user" name="email_user" placeholder="邮箱登录名,如mfkwfc@gmail.com" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="email_pas">邮箱登录密码<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="email_pas" name="email_pas" placeholder="邮箱登录密码" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="email_pop">pop<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="email_pop" name="email_pop" placeholder="邮箱接收邮件服务器地址" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="email_smtp">smtp<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="email_smtp" name="email_smtp" placeholder="邮箱发送服务器地址" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="email_isopen">开启邮件发文<span class="red">*</span></label>
	     		<div class="controls">
	     			<label class="radio">
					  <input type="radio" name="email_isopen" id="email_isopen1" value="true" >
					 开启
					</label>
					<label class="radio">
					  <input type="radio" name="email_isopen" id="email_isopen2" value="false" checked>
					  关闭
					</label>
	    		</div>
	  	 </div>
	  	 <blockquote><p>前台页面设置</p></blockquote>
	  	 <div class="control-group"><label class="control-label" for="page_size">页面分页条数<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="page_size" name="page_size" placeholder="每页可以显示多少条数据" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="page_breviary">前台缩略显示长度<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="page_breviary" name="page_breviary" placeholder="前台缩略显示长度" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <blockquote><p>七牛云帐号</p></blockquote>
	  	 <div class="control-group"><label class="control-label" for="qiniu_account">帐号<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="qiniu_account" name="qiniu_account" placeholder="七牛登录帐号" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="qiniu_password">key<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="qiniu_password" name="qiniu_password" placeholder="七牛登录密码" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="qiniu_name">空间名称<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="qiniu_name" name="qiniu_name" placeholder="空间名称" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="qunui_first">上传空间前缀</label>
	     		<div class="controls">
	      			<input type="text" id="qunui_first" name="qunui_first" placeholder="上传空间前缀" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 
	  	 <div class="control-group"><label class="control-label" for="qiniu_suffix">七牛限制上传后缀<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="qiniu_suffix" name="qiniu_suffix" placeholder="七牛限制后缀,*.*" class="input-xlarge" />
	    		</div>
	  	 </div>
      	 <div class="form-actions">
		  		<button type="button" id="save" class="btn btn-primary">确定</button>
		  		<button type="reset" class="btn">重置</button>
		</div>
    </form>
      </div>
</body>
</html>