<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户个人设置</title>
<script type="text/javascript">
var user={
		updatebase:function(){
			$.post($("#tab").attr("action"),$("#tab").serialize(),function(data){
				if(data.result){
					art.dialog({content: '修改个人信息成功',lock:true}).time(1500);
				}else{
					var ther=null;
					if(data.name!=null && data.name!=""){
						ther=$("#tab :input[name='"+data.name+"']").parents(".control-group");
					}else{
						ther=$("#tab .control-group:eq(0)");
					}
					$("#tab .error").removeClass("error");
					$("#tab .help-inline").remove();
					$(ther).addClass("error");
					$(ther).find(":input:eq(0)").focus().after('<span class="help-inline">'+data.text+'</span>');
				}
			},"json");
		},updatepassword:function(){
			$.post($("#tab2").attr("action"),$("#tab2").serialize(),function(data){
				if(data.result){
					art.dialog({content: '修改个人密码成功,需要您重新登录',lock:true}).time(1500);
					$.post(getProjectName()+"/module/user_logout.action");
					setTimeout(function(){
						 location.reload();
					},1500);
				}else{
					var ther=null;
					if(data.name!=null && data.name!=""){
						ther=$("#tab2 :input[name='"+data.name+"']").parents(".control-group");
					}else{
						ther=$("#tab2 .control-group:eq(0)");
					}
					$("#tab2 .error").removeClass("error");
					$("#tab2 .help-inline").remove();
					$(ther).addClass("error");
					$(ther).find(":input:eq(0)").focus().after('<span class="help-inline">'+data.text+'</span>');
				}
			},"json");
		},loaduser:function(){
			$.post(getProjectName()+"/module/user_findUser.action",{"id":"${sessionScope.user.id}"},function(data){
				$.each(data[0],function(k,v){
					$("#tab :input[name='"+k+"']").val(v);
				});
			},"json");
		}
}
$(function(){user.loaduser();});
</script>
</head>
<body>
<div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">个人信息</a></li>
      <li class=""><a href="#profile" data-toggle="tab">修改密码</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane  active" id="home">
	    <form id="tab" action="${pageContext.request.contextPath}/module/user_setuserbase.action">
	  	 <div class="control-group"><label class="control-label" for="realname">姓名<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="realname" name="realname" placeholder="您的真实姓名,显示在系统内" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="phone">手机</label>
	     		<div class="controls">
	      			<input type="text" id="phone" name="phone" placeholder="您的手机号码" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="mail">邮箱<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="mail" name="mail" placeholder="您的邮箱,可能会接收到系统发送的邮件" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="address">地址</label>
	     		<div class="controls">
	      			<textarea id="address" name="address" placeholder="您所在的地址" class="input-xlarge"></textarea>
	    		</div>
	  	 </div>
      	 <div>
		  		<button type="button" onclick="user.updatebase()" class="btn btn-primary">修改</button>
		</div>
	    </form>
      </div>
      <div class="tab-pane" id="profile">
		    <form id="tab2" action="${pageContext.request.contextPath}/module/user_setuserpassword.action">
		    	<div class="control-group"><label class="control-label" for="pasword">旧密码<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="password" id="pasword" name="pasword" placeholder="旧登录密码" class="input-xlarge" />
	    		</div>
			  	 </div>
		       <div class="control-group"><label class="control-label" for="pasword1">登录密码<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="password" id="pasword1" name="pasword1" placeholder="登录密码,建议六位字母,数字,特殊字符组合" class="input-xlarge" />
	    		</div>
			  	 </div>
			  	 <div class="control-group"><label class="control-label" for="pasword2">重复密码<span class="red">*</span></label>
			     		<div class="controls">
			      			<input type="password" id="pasword2" name="pasword2" placeholder="重复一次登录密码" class="input-xlarge" />
			    		</div>
			  	 </div>
		        <div><button onclick="user.updatepassword()" type="button" class="btn btn-primary">修改</button></div>
		    </form>
      </div>
  </div>
</div>
</body>
</html>