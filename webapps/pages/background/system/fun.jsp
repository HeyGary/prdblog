<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<title>功能管理</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/js/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/js/jquery.dragsort-0.5.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/js/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/pagejs/system/fun.js"></script>
<script type="text/javascript">
$(function(){
	fun.loadtree();
	$("#sortul").dragsort();
})
</script>
</head>
<body>
<div id="showdata">
<div id="seach_div"  class="form-inline btn-toolbar">
    <input class="input-xlarge" name="name" placeholder="Search..." type="text">
    <button class="btn" id="fungridsearch" type="button"><i class="icon-search"></i>Seach</button>
</div>
<jsp:include page="/pages/template/include/funbut.jsp">
<jsp:param value="00010002" name="code"/>
</jsp:include>
<div class="row-fluid">
  <div class="span3">
  	<div id="ztree" class="ztree" style="width:100%;height:300px;">
  	</div>
  </div>
  <div class="span9">
  			<table sortname="funcode"
  			 sortorder="asc" 
  			 class="auto_startgrid" 
  			 id="fungrid" 
  			 action="${pageContext.request.contextPath}/module/fun_funjqgrid.action">
			<tr>
				<th name="name" width="100">名称</th>
				<th name="funcode" width="60">code</th>
				<th name="type" formatter="fun.formattertype" width="100">类型</th>
				<th name="cls" width="50">样式</th>
				<th name="link" width="150">连接</th>
			</tr>
			</table>
  </div>
</div>
</div>
<div id="myTabContent"  style="display:none;" class="tab-content">
<div class="page-header">
  <h2 style="text-indent:230px;" id="form_title"><<span id="fun_parentname"></span>>添加功能模块</h2>
</div>
    <form id="myform" action="${pageContext.request.contextPath}/module/fun_saveFun.action" class="form-horizontal">
    	<input type="hidden" name="id"/>
    	<input type="hidden" id="parentid" name="parentid"/>
	     <div class="control-group"><label class="control-label" for="name">功能名称<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="name" name="name" placeholder="功能表名称" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="type">功能类型<span class="red">*</span></label>
	     		<div class="controls">
	     		    <select id="type" onchange="if(this.value==3)$('#ftltype,#ftlpath').removeAttr('disabled');else $('#ftltype,#ftlpath').attr('disabled','disabled');" name="type"  class="input-xlarge">
	     		    	<option value="0">无</option>
	     		    	<option value="1">菜单</option>
	     		    	<option value="2">按钮</option>
	     		    	<option value="3">模版</option>
	     		    </select>
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="ftltype">模版类型<span class="red">*</span></label>
	     		<div class="controls">
	     		    <select id="ftltype" disabled="disabled" name="ftltype"  class="input-xlarge">
	     		    	<option value="0">菜单</option>
	     		    	<option value="1">单页</option>
	     		    	<option value="2">列表</option>
	     		    	<option value="3">主页</option>
	     		    	<option value="4">直连</option>
	     		    </select>
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="ftlpath">模版地址</label>
	     		<div class="controls">
	     		   <input type="text" id="ftlpath" name="ftlpath" placeholder="后台模版地址" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 
	  	 <div class="control-group"><label class="control-label" for="noteid">功能ID</label>
	     		<div class="controls">
	      			<input type="text" id="noteid" name="noteid" placeholder="功能id" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="cls">功能样式</label>
	     		<div class="controls">
	      			<input type="text" id="cls" name="cls" placeholder="功能样式" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="link">连接</label>
	     		<div class="controls">
	      			<input type="text" id="link" name="link" placeholder="菜单连接,或者功能事件" class="input-xlarge" />
	    		</div>
	  	 </div>
      	 <div class="form-actions">
		  		<button type="button" onclick="fun.savefun()" class="btn btn-primary">确定</button>
		  		<button type="button" onclick="fun.toggle(0)" class="btn">取消</button>
		</div>
    </form>
      </div>
 <div style="display:none;">
 <ul class="sortul" id="sortul">
 </ul>
 </div>
</body>
</html>