<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>友情连接</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/pagejs/data/link.js"></script>
</head>
<body>
<div id="showdata">
<div id="seach_div"  class="form-inline btn-toolbar">
    <input class="input-xlarge" name="name" placeholder="Search..." type="text">
    <button class="btn" id="linkgridsearch" type="button"><i class="icon-search"></i>Seach</button>
</div>
<jsp:include page="/pages/template/include/funbut.jsp">
<jsp:param value="00010005" name="code"/>
</jsp:include>
<table class="auto_startgrid" id="linkgrid" action="${pageContext.request.contextPath}/prd/link_linkjqgrid.action">
<tr>
	<th name="name" width="50%">友情网站名称</th>
	<th name="path" width="50%">连接地址</th>
</tr>
</table>
</div>
<div id="myTabContent"  style="display:none;" class="tab-content">
<div class="page-header">
  <h2 style="text-indent:230px;" id="form_title">添加友情连接</h2>
</div>
    <form id="myform" action="${pageContext.request.contextPath}/prd/link_svaelink.action" class="form-horizontal">
    	 <input type="hidden" name="id"/>
	     <div class="control-group"><label class="control-label" for="name">名称<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="name" name="name" placeholder="需要连接的名称" class="input-xlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="path">连接<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="path" name="path" placeholder="如:http://www.prdblog.com/" class="input-xlarge" />
	    		</div>
	  	 </div>
      	 <div class="form-actions">
		  		<button type="button" onclick="link.savelink()" class="btn btn-primary">确定</button>
		  		<button type="button" onclick="link.toggle(0)" class="btn">取消</button>
		 </div>
    </form>
      </div>
</body>
</html>