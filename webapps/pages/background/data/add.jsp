<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<title>添加数据</title>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/resource/js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/resource/js/ueditor/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/resource/js/ueditor/lang/zh-cn/zh-cn.js"></script>
<!-- 上传插件 -->
<link href="${pageContext.request.contextPath}/resource/js/uploadify/uploadify.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/js/uploadify/jquery.uploadify.min.js"></script>
<!-- 自动补全插件 -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/js/autocomplete/jquery.autocomplete.css"></link>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/js/autocomplete/jquery.autocomplete.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resource/pagejs/data/one.js"></script>
<style type="text/css">
.findlabel ul{padding-top: 5px;padding-left:155px;}
.findlabel li{float:left;width:auto;padding:5px 10px;cursor:pointer; border:1px #cdcdcd solid;margin-right:5px;}
.findlabel li span{color:red;margin-left:10px;}
</style>
</head>
<body>
<div class="page-header">
  <h2 style="text-align: center;" id="form_title">${param.type=="update" || param.findcode=="true"?"修改":"添加"}单条数据</h2>
</div>
<form id="myform" action="${pageContext.request.contextPath}/prd/article_saveArticle.action" class="form-horizontal">
		<input type="hidden" name="id" />
		<input type="hidden" name="cls" value="${param.code}"  />
		<input type="hidden" name="accid" id="accid"  />
		<textarea style="display:none;" name="atr_content" id="atr_content" ></textarea>
		<div style="display:${param.type=="update" || param.findcode=="true"?"none":"block"}" class="control-group"><label class="control-label" for="tid">自定义id</label>
	     		<div class="controls">
	      			<input type="text" id="tid" name="tid" placeholder="自定义id" class="input-xxlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="title">标题<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="title" name="title" placeholder="标题名称" class="input-xxlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="seo">SEO关键字<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="seo" name="seo" placeholder="SEO关键字" class="input-xxlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="keywords">文章描述<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="keywords" name="keywords" placeholder="文章描述" class="input-xxlarge" />
	    		</div>
	  	 </div>
	  	  <div class="control-group"><label class="control-label" for="discuss">评论状态<span class="red">*</span></label>
	     		<div class="controls">
		     		<select id="discuss" name="discuss">
		     			<option value="true">开启</option>
		     			<option value="false">关闭</option>
		     		</select>
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="arttext">自定义代码</label>
	     		<div class="controls">
		     		 <textarea id="arttext" name="arttext" placeholder="自定义html代码" rows="5"></textarea>
	    		</div>
	  	 </div>
	  	  <div class="control-group"><label class="control-label" for="label">文章标签</label>
	     		<div class="controls">
	      			<input type="text" id="label" placeholder="搜索已存在标签.或者新建标签" class="input-xxlarge" />
	    		</div>
	  	 </div>
	  	 <div class="control-group"><label class="control-label" for="label">已选择标签</label>
	  	 		<input type="hidden" name="lablename" id="lablename" />
	     		<div class="findlabel" class="controls">
	      			<ul id="findlabelul" class="clearfix">
	      			</ul>
	    		</div>
	  	 </div>
	  	 <script id="myEditor" type="text/plain" style="height:500px;">
		 </script>
	  	  <div class="form-actions">
		  		<button type="button" id="buttom_ok" onclick="one.saveOne()" class="btn btn-primary">确定</button>
		  		<button type="reset" onclick="one.retpath()" class="btn">返回</button>
		</div>
    </form>
<script type="text/javascript">
var thcode="${param.code}";
$(function(){
	one.editor='myEditor';
	UE.getEditor(one.editor).ready(function(editor){//代码必须在初始化编辑器后才能运行.如果并行处理可能会造成无法setContext的情况
		one.init();
		if(("${param.type}"=="update" || "${param.type}"=="find") && "${param.pid}"!="")
			one.loadone("${param.pid}");
		if("${param.type}"=="find")
			$("#buttom_ok").remove();
		if("${param.findcode}"=="true")
			one.loadcode("${param.code}");
		var th=$("#sidebar a[code=${param.code}]");
		
		var firstText=$("#sidebar > a[href='#"+$(th).parents("ul[id]").attr("id")+"']").text();
		
		var ht="";
		if(firstText!="")
			ht+="<li class=\"active\">"+firstText+"<span class=\"divider\">/</span></li>";
		
		$(th).parents("ul[id]").addClass("in");
		ht+="<li class=\"active\">"+$(th).text()+"</li>";
		$("#navigation").append(ht);
	});
});
</script>
</body>
</html>