<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>标签管理</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/pagejs/data/label.js"></script>
</head>
<body>
<div id="showdata">
<div id="seach_div"  class="form-inline btn-toolbar">
    <input class="input-xlarge" name="name" placeholder="Search..." type="text">
    <button class="btn" id="labelgridsearch" type="button"><i class="icon-search"></i>Seach</button>
</div>
<jsp:include page="/pages/template/include/funbut.jsp">
<jsp:param value="00010004" name="code"/>
</jsp:include>
<table class="auto_startgrid" id="labelgrid" action="${pageContext.request.contextPath}/prd/label_labeljqgrid.action">
<tr>
	<th name="name" width="300">标签名</th>
	<th name="number" width="60">关联文章数</th>
	<th name="addtime" width="100">添加时间</th>
</tr>
</table>
</div>
<div id="myTabContent"  style="display:none;" class="tab-content">
<div class="page-header">
  <h2 style="text-indent:230px;" id="form_title">添加标签</h2>
</div>
    <form id="myform" action="${pageContext.request.contextPath}/prd/label_saveLabel.action" class="form-horizontal">
    	<input type="hidden" name="id"/>
	     <div class="control-group"><label class="control-label" for="username">标签名<span class="red">*</span></label>
	     		<div class="controls">
	      			<input type="text" id="name" name="name" placeholder="标签的名称" class="input-xlarge" />
	    		</div>
	  	 </div>
      	 <div class="form-actions">
		  		<button type="button" onclick="label.savelabel()" class="btn btn-primary">确定</button>
		  		<button type="button" onclick="label.toggle(0)" class="btn">取消</button>
		</div>
    </form>
      </div>
</body>
</html>