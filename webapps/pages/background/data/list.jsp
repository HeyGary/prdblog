<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>列表</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/resource/pagejs/data/list.js"></script>
<script type="text/javascript">
var thcode="${param.code}";
$(function(){
		var th=$("#sidebar a[code=${param.code}]");
		var ht="<li class=\"active\">"+$("#sidebar > a[href='#"+$(th).parents("ul[id]").attr("id")+"']").text()+"<span class=\"divider\">/</span></li>";
		$(th).parents("ul[id]").addClass("in");
		ht+="<li class=\"active\">"+$(th).text()+"</li>";
		$("#navigation").append(ht);
});
</script>
</head>
<body>
<div id="showdata">
<div id="seach_div"  class="form-inline btn-toolbar">
    <input class="input-xlarge" name="name" placeholder="Search..." type="text">
    <button class="btn" id="listgridsearch" type="button"><i class="icon-search"></i>Seach</button>
</div>
<jsp:include page="/pages/template/include/funbut.jsp"/>
<table class="auto_startgrid" id="listgrid"
fixedwhere="cls='${param.code}'" 
sortname="top desc,sendtime"
sortorder="desc"
action="${pageContext.request.contextPath}/prd/article_artjqgrid.action">
<tr>
	<th name="title" width="300">文章标题</th>
	<th name="seo" width="100">seo关键字</th>
	<th name="lablename" width="100">标签</th>
	<th name="sendtime" width="100">发布时间</th>
	<th name="top" formatter="list.funtop" width="50">置顶</th>
</tr>
</table>
</div>
</body>
</html>