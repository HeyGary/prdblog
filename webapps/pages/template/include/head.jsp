<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                    <li><a href="${pageContext.request.contextPath}/pages/background/system/setting.jsp" class="hidden-phone visible-tablet visible-desktop" role="button">系统设置</a></li>
                    <li id="fat-menu" class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i>${sessionScope.user.realname}
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="${pageContext.request.contextPath}/pages/background/system/settinguser.jsp">我的帐户</a></li>
                            <li><a tabindex="-1" href="javascript:refreshftl()">重新生成页面</a></li>
                            <li class="divider"></li>
                            <li class="divider visible-phone"></li>
                            <li><a tabindex="-1" href="${pageContext.request.contextPath}/module/user_logout.action">退出</a></li>
                        </ul>
                    </li>
                    
                </ul>
                <a class="brand"  target="_blank" href="http://www.prdblog.com/"><span class="first">小米粥</span> <span class="second">prdblog</span></a>
        </div>
    </div>