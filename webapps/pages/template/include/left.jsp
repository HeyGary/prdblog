<%@page import="org.porridge.module.fun.po.Fun"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div id="sidebar" class="sidebar-nav">
         <%
         Map<String,List<Fun>> funmap=(Map<String,List<Fun>>)session.getAttribute("funmap");
         if(funmap!=null && funmap.get("index")!=null){
        	 List<Fun> fun1=funmap.get("index");
        	 if(fun1!=null){
	        	 StringBuffer stb=new StringBuffer();
	        	 for(Fun f1:fun1){
	        		 if("3".equals(f1.getFtltype()) || "4".equals(f1.getFtltype()))
	        			 continue;
	        		 String href="#"+f1.getId();
	        		 boolean bl=true;
	        		 if(f1.getLink()!=null && f1.getLink().trim().length()>0){
	        			 href=request.getContextPath()+f1.getLink();
	        			 bl=false;
	        		 }
	        		 boolean isftlf1=("2".equals(f1.getFtltype()) || "1".equals(f1.getFtltype()));
	        		 stb.append("<a "+(isftlf1?"code=\""+f1.getFuncode()+"\"":"")+" href=\""+href+(isftlf1?"?code="+f1.getFuncode():"")+((isftlf1 && "1".equals(f1.getFtltype()))?"&findcode=true":"")+"\" class=\"nav-header\" "+(bl?"data-toggle=\"collapse\"":"")+"><i class=\""+f1.getCls()+"\"></i>"+f1.getName()+"</a>");
	        		 List<Fun> fun2=funmap.get(f1.getFuncode());
	        		 stb.append("<ul id=\""+f1.getId()+"\" class=\"nav nav-list collapse\">");
	        		 if(fun2!=null){
		        		 for(Fun f2:fun2){
		        			 if("3".equals(f2.getFtltype()))continue;
		        			 
		        			 boolean isftl=("2".equals(f2.getFtltype()) || "1".equals(f2.getFtltype()));
		        			 stb.append("<li><a "+(isftl?"code=\""+f2.getFuncode()+"\"":"")+((isftl && "1".equals(f2.getFtltype()))?"&findcode=true":"")+" "+("3".equals(f2.getFtltype())?"target=\"_blank\"":"")+" href=\""+("3".equals(f2.getFtltype())?"":request.getContextPath())+f2.getLink()+(isftl?"?code="+f2.getFuncode():"")+"\">"+f2.getName()+"</a></li>");
		        		 }
	        		 }
	        		 stb.append("</ul>");
	        	 }
	        	 out.print(stb.toString());
        	 }
         }
         %>
</div>