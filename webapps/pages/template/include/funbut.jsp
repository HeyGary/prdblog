<%@page import="org.porridge.module.fun.po.Fun"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
Object obj=request.getParameter("code");
Map<String,List<Fun>> funmap=(Map<String,List<Fun>>)session.getAttribute("funmap");
if(obj!=null && funmap!=null && funmap.get(obj.toString())!=null){
	List<Fun> funbut=funmap.get(obj.toString());
	StringBuffer stb=new StringBuffer();
	for(Fun f:funbut){
		stb.append("<button onclick=\""+f.getLink()+"\" class=\""+f.getCls()+"\">"+f.getName()+"</button>");
	}
	if(funbut.size()>0)
		out.print("<div class=\"btn-toolbar\">"+stb.toString()+"</div>");
}
%>