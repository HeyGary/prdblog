<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/blue/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/blue/stylesheets/theme.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/blue/lib/font-awesome/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resource/pagecss/base.css">
<link href="${pageContext.request.contextPath}/resource/js/artDialog5.0/skins/simple.css" rel="stylesheet" />
<script src="${pageContext.request.contextPath}/resource/blue/lib/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/blue/lib/bootstrap/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/resource/js/artDialog5.0/artDialog.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="${pageContext.request.contextPath}/resource/js/jqGrid-4.4.0/jquicss/flick/jquery-ui-1.9.2.custom.css" />
<link rel="stylesheet" type="text/css" media="screen" href="${pageContext.request.contextPath}/resource/js/jqGrid-4.4.0/css/ui.jqgrid.css" />
<script src="${pageContext.request.contextPath}/resource/js/jqGrid-4.4.0/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/js/jqGrid-4.4.0/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/js/jqGrid-4.4.0/src/jquery.fmatter.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/js/util.js" type="text/javascript"></script>
<script type="text/javascript">function getProjectName(){return "${pageContext.request.contextPath}";}</script>