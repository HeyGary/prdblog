<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>porridge - <sitemesh:write property='title'></sitemesh:write></title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<jsp:include page="include/base.jsp"/>
<sitemesh:write property='head'/>
</head> 
<body>
<jsp:include page="include/head.jsp"/>
<jsp:include page="include/left.jsp"/>
    <div class="content">
         <ul id="navigation" class="breadcrumb">
            <li><a href="${pageContext.request.contextPath}/pages/background/index.jsp">主页</a><span class="divider">/</span></li>
        </ul>
        <div class="container-fluid">
	        <sitemesh:write property='body'></sitemesh:write>
	        <jsp:include page="include/buttom.jsp"/>
        </div>
    </div>
</body>
</html>


